/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
//#include <asf.h>
//#include <ioport.h>

#define MY_LED  1

static void delay(int delay) {
	for (int i = 0; i < delay; i++) {
		__asm (
		"NOP"
		);
	}
}

static void init(void) {
	//Data direction DDRA
	unsigned char *DDRA = (unsigned char*)0x3a;
	*DDRA = (1 << MY_LED);
}

int main (void)
{
	unsigned char *PORTA = (unsigned char*)0x3b;
	init();
	int d = 0;
	while (1)
	{
		// PIN on
		*PORTA = (1 << MY_LED);
		delay(d);
		// PIN off
		*PORTA = 0;//(0x01u << MY_LED);
		delay(d++);
		if (d > 1000000) {
			d = 0;
		}
	}
}
