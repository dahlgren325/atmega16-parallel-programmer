/*
 * Atmega16TestApp.c
 *
 * Created: 2016-11-23 22:48:03
 * Author : Ronnie
 */ 

#include <avr/io.h>
#include <ioport.h>

#define MY_LED    IOPORT_CREATE_PIN(PORTA, 5)

static void delay(int delay) {
	for (int i = 0; i < delay; i++) {
		__asm (
			"NOP"
		);
	}
}

static void init(void) {
	ioport_init();
	ioport_set_pin_dir(MY_LED, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(MY_LED, IOPORT_PIN_LEVEL_LOW);
	ioport_enable_pin(MY_LED);
}

/*int main(void)
{
	int d = 0;
    while (1) 
    {	
		ioport_set_pin_level(MY_LED, true);
		delay(d);
		ioport_set_pin_level(MY_LED, false);
		delay(d++);
		if (d > 1000000) {
			d = 0;
		}
    }
}*/

