/*
 * avr_prog.h
 *
 * Created: 2016-11-18 23:37:01
 *  Author: Ronnie
 */ 

#ifndef AVR_PROG_H_
#define AVR_PROG_H_

#include <delay.h>


#define A_PD1 PIO_PB2_IDX
#define A_PD2 PIO_PB3_IDX
#define A_PD3 PIO_PA24_IDX
#define A_PD4 PIO_PA25_IDX
#define A_PD5 PIO_PA15_IDX
#define A_PD6 PIO_PA16_IDX
#define A_PD7 PIO_PA11_IDX
#define A_RESET PIO_PD29_IDX
#define A_RESET_VOLTAGE_CONTROL PIO_PD21_IDX
#define A_PA0 PIO_PD25_IDX
#define A_XTAL1 PIO_PA3_IDX
#define A_PB0 PIO_PA4_IDX
#define A_PB1 PIO_PA21_IDX
#define A_PB2 PIO_PA22_IDX
#define A_PB3 PIO_PB14_IDX
#define A_PB4 PIO_PA13_IDX
#define A_PB5 PIO_PA12_IDX
#define A_PB6 PIO_PA14_IDX
#define A_PB7 PIO_PE2_IDX
#define A_POWER PIO_PB5_IDX

#define A_RDY PIO_PB2_IDX
#define A_OE PIO_PB3_IDX
#define A_WR PIO_PA24_IDX
#define A_BS1 PIO_PA25_IDX
#define A_XA0 PIO_PA15_IDX
#define A_XA1 PIO_PA16_IDX
#define A_PAGEL PIO_PA11_IDX
#define A_BS2 PIO_PD25_IDX

#define A_PE0 PIO_PA25_IDX
#define A_PE1 PIO_PA15_IDX
#define A_PE2 PIO_PA16_IDX
#define A_PE3 PIO_PA11_IDX

void avr_prog_init(void);
void avr_prog_initprog(void);
void avr_prog_free_buffer(void);
void avr_prog_set_buffer(char *buf, int size);
void avr_prog_write_buffer_flash(int address);
void avr_prog_chiperase(void);
void avr_prog_end_flash_prog(void);
unsigned char avr_prog_read_signature(unsigned char byteNo);
void avr_prog_testallpins(void);
void avr_prog_testpin(int pin);
uint16_t avr_prog_read_flash(int address);
uint32_t avr_prog_read_fuse(void);
void avr_prog_write_high_fuse(uint8_t);
void avr_prog_write_low_fuse(uint8_t);
void avr_prog_write_lock_bits(uint8_t);
void avr_prog_set_reset_high_voltage(bool highVoltage);
void avr_prog_reset(void);
void avr_prog_reset_low(void);

#endif /* AVR_PROG_H_ */