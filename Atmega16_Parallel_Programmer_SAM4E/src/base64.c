/*
 * base64.c
 *
 * Created: 2016-11-19 10:56:42
 *  Author: Ronnie
 */ 

#include <stdlib.h>
#include <string.h>
#include <compiler.h>
#include "base64.h"

const char *v = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

static char getEVal(char *input, int i, int length) {
	if (i > length - 1) {
		return 0;
	} else {
		if (input[i] < 0) {
			return input[i] + 256;
		} else {
			return input[i];
		}
	}
};

static int getDVal(const char *input, int i, int maxLength) {
	if (i >= maxLength) return 64;
	char *p = strchr(v, input[i++]);
	if (p == NULL) {
		return 64;
	}
	return p - v;
  };


char* b64decode(const char* input, int* size) {
	 int bits, i, o1, o2, o3, o4;
	 *size = 0;
	 i = 0;
	 int length = strlen(input);
	 if (length == 0) {
		 return 0;
	 }
	 char *r = malloc(length * 3/4 + 5);
	 while (i < length) {
		 if ((input[i] < 'A' || input[i] > 'Z') && (input[i] < 'a' || input[i] > 'z') && (input[i] < '0' || input[i] > '9') && input[i] != '+' && input[i] != '/' && input[i] != '=') {
			break;
		 }
		 o1 = getDVal(input, i++, length);
		 o2 = getDVal(input, i++, length);
		 o3 = getDVal(input, i++, length);
		 o4 = getDVal(input, i++, length);
		 bits = o1 << 18 | o2 << 12 | o3 << 6 | o4;
		 r[(*size)++] = bits >> 16 & 0xff;
		 if (o3 != 64) {
			 r[(*size)++] = bits >> 8 & 0xff;
		 }
		 if (o4 != 64) {
			 r[(*size)++] = bits & 0xff;
		 }
	 }
	 return r;
 };

void b64encode(char *input, char* out, int length) {
	int bits, o1, o2, o3;
	o1 = getEVal(input, 0, length);
	o2 = getEVal(input, 1, length);
	o3 = getEVal(input, 2, length);
	bits = o1 << 16 | o2 << 8 | o3;
	out[0] = v[bits >> 18 & 0x3f];
	out[1] = v[bits >> 12 & 0x3f];
	out[2] = v[bits >> 6 & 0x3f];
	out[3] = v[bits & 0x3f];
};
