/*
 * base64.h
 *
 * Created: 2016-11-19 10:56:21
 *  Author: Ronnie
 */ 


#ifndef BASE64_H_
#define BASE64_H_

char* b64decode(const char* input, int* size);
void b64encode(char *input, char* out, int length);

#endif /* BASE64_H_ */