/*
 * avr_serial_prog.h
 *
 * Created: 2016-11-28 01:55:42
 *  Author: Ronnie
 */


#ifndef AVR_SERIAL_PROG_H_
#define AVR_SERIAL_PROG_H_

bool avr_serial_prog_init(void);
void avr_serial_prog_end(void);
void avr_serial_prog_write_low_fuse(char fuse);
void avr_serial_prog_write_high_fuse(char fuse);
void avr_serial_prog_write_lock_bits(char lockbits);
char avr_serial_prog_read_low_fuse(void);
char avr_serial_prog_read_high_fuse(void);
char avr_serial_prog_read_lock_bits(void);
void avr_serial_prog_write_word(int address, int data);
void avr_serial_prog_write_page(int address);
void avr_serial_prog_set_buffer(char *buf, int size);
void avr_serial_prog_free_buffer(void);
void avr_serial_prog_write_buffer_flash(int address);
char avr_serial_prog_read_signature(char address);
void avr_serial_prog_chiperase(void);
uint16_t avr_serial_prog_read_flash(int address);

#endif /* AVR_SERIAL_PROG_H_ */