/*
 * jtag_prog.h
 *
 * Created: 2016-12-08 19:15:59
 *  Author: Ronnie
 */ 


#ifndef JTAG_PROG_H_
#define JTAG_PROG_H_

void jtag_prog_init(void);
void jtag_prog_set_delay(int d);
uint32_t jtag_prog_read_id_code(int deviceNo);
int jtag_prog_detect_number_of_devices(void);

#endif /* JTAG_PROG_H_ */