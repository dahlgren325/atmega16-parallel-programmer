/*
 * avr_prog.c
 *
 * Created: 2016-11-18 23:36:45
 *  Author: Ronnie
 */ 

#include "avr_prog.h"
#include "ioport.h"
#include <stdlib.h>
#include <delay.h>

static char *buffer = 0;
static int bufferSize = 0;

/*static void initTimerCounter(void) {
	//Enable PCK0
	PMC->PMC_SCER |= PMC_SCER_PCK0;// | PMC_SCER_PCK1 | PMC_SCER_PCK2;
	//Enable TC0 clock
	PMC->PMC_PCER0 |= PMC_PCER0_PID21;
	//Enable TC1 clock
	//PMC->PMC_PCER0 |= PMC_PCER0_PID22;
	//Enable TC2 clock
	//PMC->PMC_PCER0 |= PMC_PCER0_PID23;
	//Enable TC0
	REG_TC0_CCR0 |= TC_CCR_CLKEN;
	REG_TC0_CCR0 &= ~TC_CCR_CLKDIS;
	//Reset TC0
	REG_TC0_CCR0 |= TC_CCR_SWTRG;
	//Select TIMER_CLOCK1 (MCK/2)
	REG_TC0_CMR0 = (REG_TC0_CMR0 & ~TC_CMR_TCCLKS_Msk) | TC_CMR_TCCLKS_TIMER_CLOCK1;
	//Non inverting
	REG_TC0_CMR0 &= ~TC_CMR_CLKI;
	//No burst
	REG_TC0_CMR0 = (REG_TC0_CMR0 & ~TC_CMR_BURST_Msk) | TC_CMR_BURST_NONE;
	//Various
	REG_TC0_CMR0 &= ~TC_CMR_LDBSTOP;
	REG_TC0_CMR0 &= ~TC_CMR_LDBDIS;
	REG_TC0_CMR0 &= ~TC_CMR_CPCTRG;
	REG_TC0_CMR0 = (REG_TC0_CMR0 & ~TC_CMR_EEVT_Msk) | TC_CMR_EEVTEDG_NONE;
	REG_TC0_CMR0 &= ~TC_CMR_ENETRG;
	//Capture mode
	REG_TC0_CMR0 |= TC_CMR_WAVE;
}*/

/*static void initSlowClock(void) {
	REG_RTT_MR &= ~RTT_MR_RTTINCIEN;
	//REG_RTT_MR = RTT_MR_RTPRES(0x0000) | RTT_MR_ALMIEN | RTT_MR_RTTINCIEN | RTT_MR_RTTRST | RTT_MR_RTTDIS | RTT_MR_RTC1HZ;
	REG_RTT_MR = RTT_MR_RTPRES(0x0004) | RTT_MR_RTTRST;
}*/

//OPTIMIZE_HIGH
RAMFUNC
static void wait_loop(unsigned int val) {
	UNUSED(val);

	__asm (
	"loop: DMB	\n"
	"SUBS R0, R0, #1  \n"
	"BNE.N loop         "
	);
}

//1 ~ 9 us
//100 ~ 15 us
//300 ~ 44 us
//500 ~ 73 us
//700 ~ 102 us
//900 ~ 132 us
//1000 ~ 146 us
OPTIMIZE_HIGH
//RAMFUNC
static void wait(unsigned int val) {
	wait_loop(val);
	
	//if (ns < 1000) ns = 1000;
	//cpu_delay_us((double)ns / 1000.0, F_CPU);

	//uint32_t waitUntil = REG_RTT_VR + ns * 8;
	//while (REG_RTT_VR < waitUntil) {}
}

void avr_prog_set_reset_high_voltage(bool highVoltage) {
	if (highVoltage) {
		ioport_set_pin_level(A_RESET_VOLTAGE_CONTROL, IOPORT_PIN_LEVEL_LOW);
	} else {
		ioport_set_pin_level(A_RESET_VOLTAGE_CONTROL, IOPORT_PIN_LEVEL_HIGH);
	}
}

void avr_prog_set_buffer(char *buf, int size) {
	avr_prog_free_buffer();
	if (size != 0) {
		buffer = buf;
		bufferSize = size;
	}
}

void avr_prog_free_buffer(void) {
	if (bufferSize != 0) {
		free(buffer);
		bufferSize = 0;
	}
}

void avr_prog_init(void) {
	//Enable PB4
	//REG_CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;
	//REG_PIOB_PER |= PIO_PER_P4;

	//Enable PB5
	REG_CCFG_SYSIO |= CCFG_SYSIO_SYSIO5;
	REG_PIOB_PER |= PIO_PER_P5;
	//REG_PIOB_PUDR |= PIO_PUDR_P5;

	//Enable PE2
	REG_PIOE_PER |= PIO_PER_P2;
	REG_PIOE_PUDR |= PIO_PUDR_P2;

	ioport_set_pin_dir(A_PD1, IOPORT_DIR_INPUT);
	ioport_set_pin_dir(A_PD2, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PD3, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PD4, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PD5, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PD6, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PD7, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_RESET, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_RESET_VOLTAGE_CONTROL, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_XTAL1, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PB0, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PB1, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PB2, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PB3, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PB4, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PB5, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PB6, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PB7, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_PA0, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_WR, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_POWER, IOPORT_DIR_OUTPUT);

	ioport_set_pin_level(A_POWER, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_WR, IOPORT_PIN_LEVEL_HIGH);
	ioport_set_pin_level(A_XTAL1, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_BS2, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_OE, IOPORT_PIN_LEVEL_HIGH);
	ioport_set_pin_level(A_PAGEL, IOPORT_PIN_LEVEL_LOW);
	avr_prog_set_reset_high_voltage(false);

	//initSlowClock();
	//initTimerCounter();
}

static void setData(int data) {
	ioport_set_pin_level(A_PB0, ((data & 0x001) == 0x001) ? IOPORT_PIN_LEVEL_HIGH: IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PB1, ((data & 0x002) == 0x002) ? IOPORT_PIN_LEVEL_HIGH: IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PB2, ((data & 0x004) == 0x004) ? IOPORT_PIN_LEVEL_HIGH: IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PB3, ((data & 0x008) == 0x008) ? IOPORT_PIN_LEVEL_HIGH: IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PB4, ((data & 0x010) == 0x010) ? IOPORT_PIN_LEVEL_HIGH: IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PB5, ((data & 0x020) == 0x020) ? IOPORT_PIN_LEVEL_HIGH: IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PB6, ((data & 0x040) == 0x040) ? IOPORT_PIN_LEVEL_HIGH: IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PB7, ((data & 0x080) == 0x080) ? IOPORT_PIN_LEVEL_HIGH: IOPORT_PIN_LEVEL_LOW);
}

static void setDataDirection(int dir) {
	ioport_set_pin_dir(A_PB0, dir);
	ioport_set_pin_dir(A_PB1, dir);
	ioport_set_pin_dir(A_PB2, dir);
	ioport_set_pin_dir(A_PB3, dir);
	ioport_set_pin_dir(A_PB4, dir);
	ioport_set_pin_dir(A_PB5, dir);
	ioport_set_pin_dir(A_PB6, dir);
	ioport_set_pin_dir(A_PB7, dir);
	if (dir == IOPORT_DIR_INPUT) {
		setData(0);
		ioport_set_pin_level(A_OE, IOPORT_PIN_LEVEL_LOW);
	} else {
		ioport_set_pin_level(A_OE, IOPORT_PIN_LEVEL_HIGH);
	}
}

static uint8_t readData(void) {
	unsigned int data = 0;
	data |= ioport_get_pin_level(A_PB0);
	data |= (ioport_get_pin_level(A_PB1) << 1);
	data |= (ioport_get_pin_level(A_PB2) << 2);
	data |= (ioport_get_pin_level(A_PB3) << 3);
	data |= (ioport_get_pin_level(A_PB4) << 4);
	data |= (ioport_get_pin_level(A_PB5) << 5);
	data |= (ioport_get_pin_level(A_PB6) << 6);
	data |= (ioport_get_pin_level(A_PB7) << 7);
	//if ((REG_PIOB_PDSR & PIO_PDSR_P7) == PIO_PDSR_P7) {
//		data += 128;
//	}
	return data;
}

static void commandLoadAddress(void) {
	ioport_set_pin_level(A_XA1, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_XA0, IOPORT_PIN_LEVEL_LOW);
}

static void commandLoadCommand(void) {
	ioport_set_pin_level(A_XA1, IOPORT_PIN_LEVEL_HIGH);
	ioport_set_pin_level(A_XA0, IOPORT_PIN_LEVEL_LOW);
}

static void commandLoadData(void) {
	ioport_set_pin_level(A_XA1, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_XA0, IOPORT_PIN_LEVEL_HIGH);
}

static void pulseXtal1(void) {
	wait(100);
	ioport_set_pin_level(A_XTAL1, IOPORT_PIN_LEVEL_HIGH);
	wait(100);
	ioport_set_pin_level(A_XTAL1, IOPORT_PIN_LEVEL_LOW);
	wait(100);
}

static void pulseWR(void) {
	wait(100);
	ioport_set_pin_level(A_WR, IOPORT_PIN_LEVEL_LOW);
	wait(100);
	ioport_set_pin_level(A_WR, IOPORT_PIN_LEVEL_HIGH);
	wait(100);
}

static void pulsePAGEL(void) {
	wait(100);
	ioport_set_pin_level(A_PAGEL, IOPORT_PIN_LEVEL_HIGH);
	wait(100);
	ioport_set_pin_level(A_PAGEL, IOPORT_PIN_LEVEL_LOW);
	wait(100);
}

void avr_prog_chiperase(void) {
	commandLoadCommand();
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
	//DATA=1000 0000
	setData(0x80);
	//XTAL pulse
	pulseXtal1();
	//WR negative pulse
	pulseWR();
	//RDY goes low, wait until high
	while (!ioport_get_pin_level(A_RDY)) {}
}

static void latchWord(int address, int bufferOffset) {
	//B. Load Address Low byte
	commandLoadAddress();
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
	//Set DATA = Address Low byte ($00 - $FF).
	setData(address + bufferOffset / 2);
	//Give XTAL1 a positive pulse. This loads the address Low byte.
	pulseXtal1();
	//C. Load Data Low Byte
	//Set XA1, XA0 to �01�. This enables data loading.
	commandLoadData();
	//Set DATA = Data Low byte ($00 - $FF).
	setData(buffer[bufferOffset]);
	//Give XTAL1 a positive pulse. This loads the data byte.
	pulseXtal1();
	//D. Load Data High Byte
	//Set BS1 to �1�. This selects high data byte.
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_HIGH);
	//Set DATA = Data High byte ($00 - $FF).
	setData(buffer[bufferOffset + 1]);
	//Give XTAL1 a positive pulse. This loads the data byte.
	pulseXtal1();
	//E. Latch Data
	//Set BS1 to �1�. This selects high data byte.
	//Give PAGEL a positive pulse. This latches the data bytes. (See Figure 129 for signal waveforms)
	pulsePAGEL();
	//ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
	//F. Repeat B through E until the entire buffer is filled or until all data within the page is loaded.
}

void avr_prog_write_buffer_flash(int address) {
	if (bufferSize == 0) {
		return;
	}
	commandLoadCommand();
	//Set DATA to �0001 0000�. This is the command for Write Flash.
	setData(0x10);
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
	//Give XTAL1 a positive pulse. This loads the command.
	pulseXtal1();

	int bufferOffset = 0;
	while (bufferOffset < bufferSize) {
		latchWord(address, bufferOffset);
		bufferOffset += 2;
	}

	//G. Load Address High byte
	commandLoadAddress();
	//Set BS1 to �1�. This selects high address.
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_HIGH);
	//Set DATA = Address High byte ($00 - $FF).
	setData(address >> 8);
	//Give XTAL1 a positive pulse. This loads the address High byte.
	pulseXtal1();
	//H. Program Page
	//Set BS1 = �0�
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
	//Give WR a negative pulse. This starts programming of the entire page of data. RDY/BSY goes low.
	pulseWR();
	//Wait until RDY/BSY goes high. (See Figure 129 for signal waveforms)
	while (!ioport_get_pin_level(A_RDY)) {}
	//I. Repeat B through H until the entire Flash is programmed or until all data has been programmed.
}

void avr_prog_end_flash_prog(void) {
	//J. End Page Programming
	commandLoadCommand();
	//Set DATA to �0000 0000�. This is the command for No Operation.
	setData(0);
	//Give XTAL1 a positive pulse. This loads the command, and the internal write signals are reset.
	pulseXtal1();
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_LOW);
	avr_prog_set_reset_high_voltage(false);
	wait(10000);
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_HIGH);
}

unsigned char avr_prog_read_signature(unsigned char byteNo) {
	commandLoadCommand();
	//Read signature
	setData(0x08);
	//Give XTAL1 a positive pulse. This loads the command, and the internal write signals are reset.
	pulseXtal1();
	//Load Address Low byte
	commandLoadAddress();
	//Set BS1 to �0�. This selects low address.
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
	//Set Address
	setData(byteNo);
	//Give XTAL1 a positive pulse. This loads the address Low byte.
	pulseXtal1();
	//SET OE low to read output
	setDataDirection(IOPORT_DIR_INPUT);
	//Read low byte
	wait(100);
	uint8_t signature = readData();
	//SET OE high
	setDataDirection(IOPORT_DIR_OUTPUT);
	return signature;
}

uint32_t avr_prog_read_fuse(void) {
	commandLoadCommand();
	//Read fuse
	setData(0x04);
	//Give XTAL1 a positive pulse. This loads the command, and the internal write signals are reset.
	pulseXtal1();
	//Set OE, BS1 and BS2 to �0�.
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_BS2, IOPORT_PIN_LEVEL_LOW);
	setDataDirection(IOPORT_DIR_INPUT);
	wait(100);
	//Read data
	uint32_t lowFuse = readData();
	//Set BS1 and BS2 to �1�.
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_HIGH);
	ioport_set_pin_level(A_BS2, IOPORT_PIN_LEVEL_HIGH);
	wait(100);
	uint32_t highFuse = readData();
	//Set BS2 to 0 and BS1 to 1.
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_HIGH);
	ioport_set_pin_level(A_BS2, IOPORT_PIN_LEVEL_LOW);
	wait(100);
	uint32_t lockBits = readData();
	setDataDirection(IOPORT_DIR_OUTPUT);
	setData(0xff);
	return (lockBits << 16) | (highFuse << 8) | lowFuse;
}

void avr_prog_initprog(void) {
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_LOW);
	avr_prog_set_reset_high_voltage(true);
	ioport_set_pin_level(A_POWER, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_XTAL1, IOPORT_PIN_LEVEL_LOW);
	wait(50000);
	//Power up
	ioport_set_pin_level(A_POWER, IOPORT_PIN_LEVEL_HIGH);
	//Wait >100 us
	wait(800);
	//Toggle XTAL1 10 times
	for (int i = 0; i < 10; i++) {
		wait(100);
		ioport_set_pin_level(A_XTAL1, IOPORT_PIN_LEVEL_HIGH);
		wait(100);
		ioport_set_pin_level(A_XTAL1, IOPORT_PIN_LEVEL_LOW);
	}
	//Set PE0-PE3 to 0
	ioport_set_pin_level(A_PE0, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PE1, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PE2, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_PE3, IOPORT_PIN_LEVEL_LOW);
	// Wait >100 ns
	wait(100);
	// 12V to RESET
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_HIGH);
	// Wait >100 ns
	wait(100);
}

static void testAllPinsInternal(int value) {
	ioport_set_pin_level(A_PB2, value);
	ioport_set_pin_level(A_PD1, value);
	ioport_set_pin_level(A_PD2, value);
	ioport_set_pin_level(A_PD3, value);
	ioport_set_pin_level(A_PD4, value);
	ioport_set_pin_level(A_PD5, value);
	ioport_set_pin_level(A_PD6, value);
	ioport_set_pin_level(A_PD7, value);
	ioport_set_pin_level(A_RESET, value);
	ioport_set_pin_level(A_PA0, value);
	ioport_set_pin_level(A_XTAL1, value);
	ioport_set_pin_level(A_PB0, value);
	ioport_set_pin_level(A_PB1, value);
	ioport_set_pin_level(A_PB2, value);
	ioport_set_pin_level(A_PB3, value);
	ioport_set_pin_level(A_PB4, value);
	ioport_set_pin_level(A_PB5, value);
	ioport_set_pin_level(A_PB6, value);
	ioport_set_pin_level(A_PB7, value);
	ioport_set_pin_level(A_POWER, value);
	ioport_set_pin_level(A_RESET_VOLTAGE_CONTROL, value);
}

void avr_prog_testallpins(void) {
	testAllPinsInternal(IOPORT_PIN_LEVEL_LOW);
	wait(100);
	testAllPinsInternal(IOPORT_PIN_LEVEL_HIGH);
	wait(100);
	testAllPinsInternal(IOPORT_PIN_LEVEL_LOW);
}

void avr_prog_testpin(int pin) {
	ioport_set_pin_level(pin, IOPORT_PIN_LEVEL_LOW);
	wait(100);
	ioport_set_pin_level(pin, IOPORT_PIN_LEVEL_HIGH);
	wait(100);
	ioport_set_pin_level(pin, IOPORT_PIN_LEVEL_LOW);
}

uint16_t avr_prog_read_flash(int address) {
	commandLoadCommand();
	//Load command 0000 0010
	setData(0b00000010);
	pulseXtal1();

	//Load address high byte
	commandLoadAddress();
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_HIGH);
	int high = (address >> 8);
	setData(high);
	pulseXtal1();

	//Load address low byte
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
	int low = address & 0xff;
	setData(low);
	pulseXtal1();

	//Set OE=0 and BS1=0
	setDataDirection(IOPORT_DIR_INPUT);
	wait(100);
	//Read low byte
	uint8_t lowByte = readData();
	//Set BS1=1
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_HIGH);
	wait(100);
	//Read high byte
	uint8_t highByte = readData();
	//Set OE=1
	setDataDirection(IOPORT_DIR_OUTPUT);
	return (((uint16_t)lowByte) << 8) | ((uint16_t) highByte);
}

void avr_prog_write_high_fuse(uint8_t value) {
	//Load command 0100 0000
	commandLoadCommand();
	setData(0b01000000);
	pulseXtal1();

	setData(value);
	commandLoadData();
	pulseXtal1();

	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_HIGH);

	//Give WR a negative pulse and wait for RDY/BSY to go high
	pulseWR();
	//RDY goes low, wait until high
	while (!ioport_get_pin_level(A_RDY)) {}
	//Set BS1 to �0�. This selects low data byte.
	ioport_set_pin_level(A_BS1, IOPORT_PIN_LEVEL_LOW);
}

void avr_prog_write_low_fuse(uint8_t value) {
	//Load command 0100 0000
	commandLoadCommand();
	setData(0b01000000);
	pulseXtal1();
	commandLoadData();
	setData(value);
	pulseXtal1();
	//Give WR a negative pulse and wait for RDY/BSY to go high
	pulseWR();
	//RDY goes low, wait until high
	while (!ioport_get_pin_level(A_RDY)) {}
}

void avr_prog_write_lock_bits(uint8_t value) {
	//Load command 0010 0000
	commandLoadCommand();
	setData(0b00100000);
	pulseXtal1();
	setData(value);
	commandLoadData();
	pulseXtal1();
	//Give WR a negative pulse and wait for RDY/BSY to go high
	pulseWR();
	//RDY goes low, wait until high
	while (!ioport_get_pin_level(A_RDY)) {}
}

void avr_prog_reset(void) {
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_LOW);
	avr_prog_set_reset_high_voltage(false);
	wait(10000);
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_HIGH);
}

void avr_prog_reset_low(void) {
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_LOW);
	avr_prog_set_reset_high_voltage(false);
}