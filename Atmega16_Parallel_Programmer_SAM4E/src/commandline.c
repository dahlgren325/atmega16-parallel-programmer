/*
 * commandline.c
 *
 * Created: 2016-11-19 10:31:44
 *  Author: Ronnie
 */ 

#include "commandline.h"
#include <asf.h>
#include <string.h>
#include "avr_prog.h"
#include "base64.h"
#include <stdlib.h>
#include "avr_serial_prog.h"
#include "jtag_prog.h"

static bool inPinTestMode = false; 

static char *msgToWrite = NULL;

void commandline_flushout(void) {
	if (msgToWrite != NULL) {
		commandline_output(msgToWrite);
	}
}

void commandline_output(const char *msg) {
	int c = 0;
	char v = msg[c++];
	while (v != 0) {
		if (!udi_cdc_putc(v)) {
			msgToWrite = (char*)&msg[c - 1];
			return;
		}
		v = msg[c++];
	}
	msgToWrite = NULL;
	//udi_cdc_write_buf(msg, strlen(msg)); // bug, long messages cause hangups!
}

static void pinTestMode(char *commandline, int max) {
	int len = strnlen(commandline, max);
	if (len == 0 || strncasecmp(commandline, "\r", 1) == 0  || strncasecmp(commandline, "\n", 1) == 0) {
		const char* msg = "\r\npintest>";
		commandline_output(msg);
		return;
	}
	if (strncasecmp(commandline, "EXIT", 4) == 0) {
		const char* msg = "\r\nExited pin test mode\r\n>";
		commandline_output(msg);
		inPinTestMode = false;
		return;
	}
	if (strncasecmp(commandline, "all", 3) == 0) {
		const char* msg = "\r\nTesting all pins\r\npintest>";
		commandline_output(msg);
		return;
	}
	if (strncasecmp(commandline, "help", 4) == 0 || strncasecmp(commandline, "?", 1) == 0) {
		const char* msg = "\r\n    all - Send pulses on all pins"
		"\r\n    <number> - Send pulse on specified PIN, see sam4e16e.h PIO_PXXX_IDX and avr_prog.c"
		"\r\n    exit - Exit pintest utility"
		"\r\n    help - this help text"
		"\r\npintest>";
		commandline_output(msg);
		return;
	}
	char *endptr;
	int pin = strtol(commandline, &endptr, 10);
	if (pin >= 0 && endptr != commandline) {
		avr_prog_testpin(pin);
		const char* msg = "\r\nSending pulse on pin\r\npintest>";
		commandline_output(msg);
		return;
	}

	const char* msg = "\r\nUnrecognized command\r\npintest>";
	commandline_output(msg);
}

static bool commandline_parse_parallel(char *commandline, int max) {
	if (strncasecmp(commandline, "INIT", 4) == 0) {
		const char* msg = "\r\nInit\r\n>";
		commandline_output(msg);
		avr_prog_initprog();
		return true;
	}
	if (strncasecmp(commandline, "TRANS ", 6) == 0) {
		const char* msg = "\r\nStoring value in buffer\r\n>";
		commandline_output(msg);
		int bufsize;
		char *buf = b64decode(commandline + 6, &bufsize);
		avr_prog_set_buffer(buf, bufsize);
		return true;
	}
	if (strncasecmp(commandline, "WRITE ", 6) == 0) {
		int address = strtol(commandline + 6, NULL, 0);
		if (address < 0) {
			const char* msg = "\r\nInvalid address\r\n>";
			commandline_output(msg);
			return true;
		}
		avr_prog_write_buffer_flash(address);
		const char* msg = "\r\nWriting buffer to chip\r\n>";
		commandline_output(msg);
		return true;
	}
	if (strncasecmp(commandline, "READ ", 5) == 0) {
		char *lenPtr = NULL;
		int address = strtol(commandline + 5, &lenPtr, 0);
		if (lenPtr == commandline || *lenPtr != ' ' || lenPtr == NULL) {
			const char* msg = "\r\nUsage:\r\nREAD <address> <length>\r\n>";
			commandline_output(msg);
			return true;
		}
		if (address < 0) {
			const char* msg = "\r\nInvalid address\r\n>";
			commandline_output(msg);
			return true;
		}
		int l = strtol(lenPtr, NULL, 0);
		if (l <= 0) {
			const char* msg = "\r\nInvalid length\r\n>";
			commandline_output(msg);
			return true;
		}
		const char* tmp = "\r\nflash ";
		char msg[1000];
		strcpy(msg, tmp);
		char* outbuffer = &msg[strlen(tmp)];
		char mem[3];
		char b64[5];
		b64[4] = 0;
		int memCnt = 0;
		int outbufferCnt = 0;
		for(int lenCnt = l; lenCnt > 0; lenCnt--) {
			uint16_t d = avr_prog_read_flash(address++);
			mem[memCnt++] = (d & 0xff00) >> 8;
			if (memCnt >= 3) {
				b64encode(mem, b64, 3);
				for (int i = 0; i < 4; i++) {
					outbuffer[outbufferCnt++] = b64[i];
					if (outbufferCnt > 980) {
						outbuffer[outbufferCnt] = 0;
						commandline_output(outbuffer);
						outbufferCnt = 0;
					}
				}
				memCnt = 0;
			}
			mem[memCnt++] = (d & 0xff);
			if (memCnt >= 3) {
				b64encode(mem, b64, 3);
				for (int i = 0; i < 4; i++) {
					outbuffer[outbufferCnt++] = b64[i];
					if (outbufferCnt > 980) {
						outbuffer[outbufferCnt] = 0;
						commandline_output(outbuffer);
						outbufferCnt = 0;
					}
				}
				memCnt = 0;
			}
		}
		if (memCnt == 1) {
			b64encode(mem, b64, 1);
			b64[2] = '=';
			b64[3] = '=';
			b64[4] = 0;
			for (int i = 0; i < 4; i++) {
				outbuffer[outbufferCnt++] = b64[i];
			}
			} else if (memCnt == 2) {
			b64encode(mem, b64, 2);
			b64[3] = '=';
			b64[4] = 0;
			for (int i = 0; i < 4; i++) {
				outbuffer[outbufferCnt++] = b64[i];
			}
		}
		outbuffer[outbufferCnt++] = '\r';
		outbuffer[outbufferCnt++] = '\n';
		outbuffer[outbufferCnt++] = '>';
		outbuffer[outbufferCnt] = 0;
		commandline_output(msg);
		return true;
	}
	if (strncasecmp(commandline, "END", 3) == 0) {
		const char* msg = "\r\nEnding page programming\r\n>";
		commandline_output(msg);
		avr_prog_end_flash_prog();
		return true;
	}
	if (strncasecmp(commandline, "ERASE", 5) == 0) {
		const char* msg = "\r\nErasing chip\r\n>";
		commandline_output(msg);
		avr_prog_chiperase();
		return true;
	}
	if (strncasecmp(commandline, "SIG", 3) == 0) {
		char msg[] = "\r\nSignature: xxxxxx\r\n>";
		int s1 = avr_prog_read_signature(0);
		int s2 = avr_prog_read_signature(1);
		int s3 = avr_prog_read_signature(2);
		char n[3];
		itoa(s1, n, 16);
		if (n[1] == 0) {
			msg[13] = '0';
			memcpy(&msg[14], &n[0], 1);
			} else {
			memcpy(&msg[13], &n[0], 2);
		}
		itoa(s2, n, 16);
		if (n[1] == 0) {
			msg[15] = '0';
			memcpy(&msg[16], &n[0], 1);
			} else {
			memcpy(&msg[15], &n[0], 2);
		}
		itoa(s3, n, 16);
		if (n[1] == 0) {
			msg[17] = '0';
			memcpy(&msg[18], &n[0], 1);
			} else {
			memcpy(&msg[17], &n[0], 2);
		}
		commandline_output(msg);
		return true;
	}
	if (strncasecmp(commandline, "READFUSE", 8) == 0) {
		char msg[] = "\r\n<lock><highfuse><lowfuse>: ";
		commandline_output(msg);
		char n[3];
		n[2] = 0;
		uint32_t f = avr_prog_read_fuse();
		unsigned char b = (f & 0xff0000) >> 16;
		itoa(b, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		commandline_output(n);
		b = (f & 0x00ff00) >> 8;
		itoa(b, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		commandline_output(n);
		b = f & 0x0000ff;
		itoa(b, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		commandline_output(n);
		char msg2[] = "\r\n>";
		commandline_output(msg2);
		return true;
	}
	if (strncasecmp(commandline, "WRITEHIGHFUSE", 13) == 0) {
		int fuse = strtol(commandline + 13, NULL, 0);
		if (fuse < 0) {
			const char* msg = "\r\nInvalid fuse\r\n>";
			commandline_output(msg);
			return true;
		}
		const char* msg = "\r\nWriting high fuse to chip\r\n>";
		commandline_output(msg);
		avr_prog_write_high_fuse(fuse);
		return true;
	}
	if (strncasecmp(commandline, "WRITELOWFUSE", 12) == 0) {
		int fuse = strtol(commandline + 12, NULL, 0);
		if (fuse < 0) {
			const char* msg = "\r\nInvalid fuse\r\n>";
			commandline_output(msg);
			return true;
		}
		const char* msg = "\r\nWriting low fuse to chip\r\n>";
		commandline_output(msg);
		avr_prog_write_low_fuse(fuse);
		return true;
	}
	if (strncasecmp(commandline, "WRITELOCKBITS", 13) == 0) {
		int fuse = strtol(commandline + 13, NULL, 0);
		if (fuse < 0) {
			const char* msg = "\r\nInvalid lock bits\r\n>";
			commandline_output(msg);
			return true;
		}
		const char* msg = "\r\nWriting lock bits to chip\r\n>";
		commandline_output(msg);
		avr_prog_write_lock_bits(fuse);
		return true;
	}
	return false;
}

static bool commandline_parse_serial(char *commandline, int max) {
	if (strncasecmp(commandline, "SINIT", 5) == 0) {
		bool ok = avr_serial_prog_init();
		if (!ok) {
			const char* msg = "\r\nSerial: init - failed!\r\n>";
			commandline_output(msg);
		} else {
			const char* msg = "\r\nSerial: init - ok\r\n>";
			commandline_output(msg);
		}
		return true;
	}
	if (strncasecmp(commandline, "STRANS ", 7) == 0) {
		const char* msg = "\r\nSerial: storing value in buffer\r\n>";
		commandline_output(msg);
		int bufsize;
		char *buf = b64decode(commandline + 7, &bufsize);
		avr_serial_prog_set_buffer(buf, bufsize);
		return true;
	}
	if (strncasecmp(commandline, "SWRITE ", 7) == 0) {
		int address = strtol(commandline + 7, NULL, 0);
		if (address < 0) {
			const char* msg = "\r\nInvalid address\r\n>";
			commandline_output(msg);
			return true;
		}
		avr_serial_prog_write_buffer_flash(address);
		const char* msg = "\r\nSerial: writing buffer to chip\r\n>";
		commandline_output(msg);
		return true;
	}
	if (strncasecmp(commandline, "SREAD ", 6) == 0) {
		char *lenPtr = NULL;
		int address = strtol(commandline + 5, &lenPtr, 0);
		if (lenPtr == commandline || *lenPtr != ' ' || lenPtr == NULL) {
			const char* msg = "\r\nUsage:\r\nREAD <address> <length>\r\n>";
			commandline_output(msg);
			return true;
		}
		if (address < 0) {
			const char* msg = "\r\nInvalid address\r\n>";
			commandline_output(msg);
			return true;
		}
		int l = strtol(lenPtr, NULL, 0);
		if (l <= 0) {
			const char* msg = "\r\nInvalid length\r\n>";
			commandline_output(msg);
			return true;
		}
		const char* tmp = "\r\nflash ";
		char msg[1000];
		strcpy(msg, tmp);
		char* outbuffer = &msg[strlen(tmp)];
		char mem[3];
		char b64[5];
		b64[4] = 0;
		int memCnt = 0;
		int outbufferCnt = 0;
		for(int lenCnt = l; lenCnt > 0; lenCnt--) {
			uint16_t d = avr_serial_prog_read_flash(address++);
			mem[memCnt++] = (d & 0xff00) >> 8;
			if (memCnt >= 3) {
				b64encode(mem, b64, 3);
				for (int i = 0; i < 4; i++) {
					outbuffer[outbufferCnt++] = b64[i];
					if (outbufferCnt > 980) {
						outbuffer[outbufferCnt] = 0;
						commandline_output(outbuffer);
						outbufferCnt = 0;
					}
				}
				memCnt = 0;
			}
			mem[memCnt++] = (d & 0xff);
			if (memCnt >= 3) {
				b64encode(mem, b64, 3);
				for (int i = 0; i < 4; i++) {
					outbuffer[outbufferCnt++] = b64[i];
					if (outbufferCnt > 980) {
						outbuffer[outbufferCnt] = 0;
						commandline_output(outbuffer);
						outbufferCnt = 0;
					}
				}
				memCnt = 0;
			}
		}
		if (memCnt == 1) {
			b64encode(mem, b64, 1);
			b64[2] = '=';
			b64[3] = '=';
			b64[4] = 0;
			for (int i = 0; i < 4; i++) {
				outbuffer[outbufferCnt++] = b64[i];
			}
			} else if (memCnt == 2) {
			b64encode(mem, b64, 2);
			b64[3] = '=';
			b64[4] = 0;
			for (int i = 0; i < 4; i++) {
				outbuffer[outbufferCnt++] = b64[i];
			}
		}
		outbuffer[outbufferCnt++] = '\r';
		outbuffer[outbufferCnt++] = '\n';
		outbuffer[outbufferCnt++] = '>';
		outbuffer[outbufferCnt] = 0;
		commandline_output(msg);
		return true;
	}
	if (strncasecmp(commandline, "SEND", 4) == 0) {
		const char* msg = "\r\nSerial: ending page programming\r\n>";
		commandline_output(msg);
		avr_serial_prog_end();
		return true;
	}
	if (strncasecmp(commandline, "SERASE", 6) == 0) {
		const char* msg = "\r\nSerial: erasing chip\r\n>";
		commandline_output(msg);
		avr_serial_prog_chiperase();
		return true;
	}
	if (strncasecmp(commandline, "SSIG", 4) == 0) {
		char msg[] = "\r\nSignature: xxxxxx\r\n>";
		int s1 = avr_serial_prog_read_signature(0);
		int s2 = avr_serial_prog_read_signature(1);
		int s3 = avr_serial_prog_read_signature(2);
		char n[3];
		itoa(s1, n, 16);
		if (n[1] == 0) {
			msg[13] = '0';
			memcpy(&msg[14], &n[0], 1);
			} else {
			memcpy(&msg[13], &n[0], 2);
		}
		itoa(s2, n, 16);
		if (n[1] == 0) {
			msg[15] = '0';
			memcpy(&msg[16], &n[0], 1);
			} else {
			memcpy(&msg[15], &n[0], 2);
		}
		itoa(s3, n, 16);
		if (n[1] == 0) {
			msg[17] = '0';
			memcpy(&msg[18], &n[0], 1);
			} else {
			memcpy(&msg[17], &n[0], 2);
		}
		commandline_output(msg);
		return true;
	}
	if (strncasecmp(commandline, "SREADFUSE", 9) == 0) {
		char msg[] = "\r\n<lock><highfuse><lowfuse>: ";
		commandline_output(msg);
		char n[3];
		n[2] = 0;
		uint32_t f = avr_serial_prog_read_lock_bits();
		itoa(f, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		commandline_output(n);
		f = avr_serial_prog_read_high_fuse();
		itoa(f, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		commandline_output(n);
		f = avr_serial_prog_read_low_fuse();
		itoa(f, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		commandline_output(n);
		char msg2[] = "\r\n>";
		commandline_output(msg2);
		return true;
	}
	if (strncasecmp(commandline, "SWRITEHIGHFUSE", 14) == 0) {
		int fuse = strtol(commandline + 14, NULL, 0);
		if (fuse < 0) {
			const char* msg = "\r\nInvalid fuse\r\n>";
			commandline_output(msg);
			return true;
		}
		const char* msg = "\r\nSerial: writing high fuse to chip\r\n>";
		commandline_output(msg);
		avr_serial_prog_write_high_fuse(fuse);
		return true;
	}
	if (strncasecmp(commandline, "SWRITELOWFUSE", 13) == 0) {
		int fuse = strtol(commandline + 13, NULL, 0);
		if (fuse < 0) {
			const char* msg = "\r\nInvalid fuse\r\n>";
			commandline_output(msg);
			return true;
		}
		const char* msg = "\r\nSerial: writing low fuse to chip\r\n>";
		commandline_output(msg);
		avr_serial_prog_write_low_fuse(fuse);
		return true;
	}
	if (strncasecmp(commandline, "SWRITELOCKBITS", 14) == 0) {
		int fuse = strtol(commandline + 14, NULL, 0);
		if (fuse < 0) {
			const char* msg = "\r\nInvalid lock bits\r\n>";
			commandline_output(msg);
			return true;
		}
		const char* msg = "\r\nSerial: writing lock bits to chip\r\n>";
		commandline_output(msg);
		avr_serial_prog_write_lock_bits(fuse);
		return true;
	}
	return false;
}

static bool commandline_parse_jtag(char *commandline, int max) {
	if (strncasecmp(commandline, "jinit", 5) == 0) {
		jtag_prog_init();
		const char* msg = "\r\nJTAG init\r\n>";
		commandline_output(msg);
		return true;
	}
	if (strncasecmp(commandline, "jcount", 6) == 0) {
		int cnt = jtag_prog_detect_number_of_devices();
		char buf1[10];
		char buf2[50];
		itoa(cnt, buf1, 10);
		const char* msg1 = "\r\nFound ";
		const char* msg2 = " devices \r\n>";
		strcat(buf2, msg1);
		strcat(buf2, buf1);
		strcat(buf2, msg2);
		commandline_output(buf2);
		return true;
	}
	if (strncasecmp(commandline, "jid ", 4) == 0) {
		char *lenPtr = NULL;
		int deviceNo = strtol(commandline + 4, &lenPtr, 0);
		if (lenPtr == commandline || lenPtr == NULL) {
			const char* msg = "\r\nUsage:\r\nJID <device number>\r\n>";
			commandline_output(msg);
			return true;
		}
		if (deviceNo < 0) {
			const char* msg = "\r\nInvalid device number\r\n>";
			commandline_output(msg);
			return true;
		}
		char buf[100];
		strcat(buf, "\r\nId: 0x");
		uint32_t id = jtag_prog_read_id_code(deviceNo);
		char n[3];
		n[2] = 0;
		itoa((id & 0xff000000) >> 24, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		strcat(buf, n);
		itoa((id & 0x00ff0000) >> 16, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		strcat(buf, n);
		itoa((id & 0x0000ff00) >> 8, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		strcat(buf, n);
		itoa(id & 0x000000ff, n, 16);
		if (n[1] == 0) {
			n[1] = n[0];
			n[0] = '0';
		}
		strcat(buf, n);
		strcat(buf, "\r\n>");
		commandline_output(buf);
		return true;
	}
	if (strncasecmp(commandline, "jdelay ", 7) == 0) {
		int delay = strtol(commandline + 7, NULL, 0);
		if (delay < 1) {
			const char* msg = "\r\nInvalid delay\r\n>";
			commandline_output(msg);
			return true;
		}
		jtag_prog_set_delay(delay);
		const char* msg = "\r\nJTAG: setting delay\r\n>";
		commandline_output(msg);
		return true;
	}
	return false;
}

static bool commandline_parse_general(char *commandline, int max) {
	if (strncasecmp(commandline, "RESETLOW", 8) == 0) {
		avr_prog_reset_low();
		const char* msg = "\r\nReset low\r\n>";
		commandline_output(msg);
		return true;
	}
	if (strncasecmp(commandline, "RESET", 5) == 0) {
		avr_prog_reset();
		const char* msg = "\r\nChip reset\r\n>";
		commandline_output(msg);
		return true;
	}
	return false;
}

void commandline_parse(char *commandline, int max) {
	if (inPinTestMode) {
		pinTestMode(commandline, max);
		return;
	}
	int len = strnlen(commandline, max);
	if (len == 0 || strncasecmp(commandline, "\r", 1) == 0  || strncasecmp(commandline, "\n", 1) == 0) {
		const char* msg = "\r\n>";
		commandline_output(msg);
		return;
	}
	if (strncasecmp(commandline, "help", 4) == 0 || strncasecmp(commandline, "?", 1) == 0) {
		const char* msg = "\r\nParallel programming utility for AVR MCUs"
		"\r\n"
		"\r\nParallel commands:"
		"\r\n    init - Put AVR into parallel programming mode"
		"\r\n    erase - Send chip erase to AVR"
		"\r\n    trans <base64 encoded data> - Latch data page for programming"
		"\r\n    write <address> - Write latched data to page at <address>"
		"\r\n    end - End AVR parallel programming mode"
		"\r\n    sig - Read signature bytes from AVR"
		"\r\n    read <address> <length> - Read <length> number of words from flash"
		"\r\n    readfuse - Read fuse and lock bits"
		"\r\n    writehighfuse <fuse> - Write high fuse"
		"\r\n    writelowfuse <fuse> - Write low fuse"
		"\r\n    writelockbits <lockbits> - Write lock bits"
		"\r\n"
		"\r\nSerial commands:"
		"\r\n    sinit - Put AVR into serial programming mode"
		"\r\n    serase - Send chip erase to AVR"
		"\r\n    strans <base64 encoded data> - Latch data page for programming"
		"\r\n    swrite <address> - Write latched data to page at <address>"
		"\r\n    send - End AVR serial programming mode"
		"\r\n    ssig - Read signature bytes from AVR"
		"\r\n    sread <address> <length> - Read <length> number of words from flash"
		"\r\n    sreadfuse - Read fuse and lock bits"
		"\r\n    swritehighfuse <fuse> - Write high fuse"
		"\r\n    swritelowfuse <fuse> - Write low fuse"
		"\r\n    swritelockbits <lockbits> - Write lock bits"
		"\r\n"
		"\r\nJTAG commands:"
		"\r\n    jinit - Init JTAG programming mode"
		"\r\n    jcount - Detect number of devices"
		"\r\n    jid <device number>- Get device ID"
		"\r\n    jdelay <delay> - Set delay (higher value -> lower frequency)"
		"\r\n"
		"\r\nGeneral commands:"
		"\r\n    reset - Send low pulse on reset pin"
		"\r\n    resetlow - Pull reset low"
		"\r\n    pintest - Test utility to send pulses on output pins"
		"\r\n    ping - Pong"
		"\r\n    help - This help text"
		"\r\n>";
		commandline_output(msg);
		return;
	}
	if (strncasecmp(commandline, "pintest", 7) == 0) {
		inPinTestMode = true;
		const char* msg = "\r\nEntering pin test mode\r\npintest>";
		commandline_output(msg);
		return;
	}
	if (strncasecmp(commandline, "ping", 4) == 0) {
		const char* msg = "\r\nPong\r\n>";
		commandline_output(msg);
		return;
	}
	if (commandline_parse_parallel(commandline, max))
		return;
	if (commandline_parse_serial(commandline, max))
		return;
	if (commandline_parse_jtag(commandline, max))
	return;
	if (commandline_parse_general(commandline, max))
	return;
	const char* msg = "\r\nUnrecognized command\r\n>";
	commandline_output(msg);
}

