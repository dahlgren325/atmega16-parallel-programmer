/*
 * jtag_prog.c
 *
 * Created: 2016-12-08 19:15:45
 *  Author: Ronnie
 */ 

#include "ioport.h"
#include "jtag_prog.h"
#include "avr_prog.h"

#define A_TDI PIO_PB2_IDX // Ext1 pin 3 -> A_PC5 pin 27
#define A_TDO PIO_PA24_IDX // Ext1 pin 5 -> A_PC4 pin 26
#define A_TMS PIO_PA15_IDX // Ext1 pin 7 -> A_PC3 pin 25
#define A_TCK PIO_PE2_IDX // Ext2 pin 5 -> A_PC2 pin 24

static int delay = 100;

RAMFUNC
static void wait_loop(unsigned int val) {
	UNUSED(val);

	__asm (
	"loop: DMB	\n"
	"SUBS R0, R0, #1  \n"
	"BNE.N loop         "
	);
}

OPTIMIZE_HIGH
static void wait(unsigned int val) {
	wait_loop(val);
}

static void pulse_sck(void) {
	ioport_set_pin_level(A_TCK, IOPORT_PIN_LEVEL_HIGH);
	wait(delay);
	ioport_set_pin_level(A_TCK, IOPORT_PIN_LEVEL_LOW);
	wait(delay);
}

static void send_state_machine_bit(bool c) {
	if (c) {
		ioport_set_pin_level(A_TMS, IOPORT_PIN_LEVEL_HIGH);
	} else {
		ioport_set_pin_level(A_TMS, IOPORT_PIN_LEVEL_LOW);
	}
	pulse_sck();
}

static void jtag_reset(void) {
	for (int i = 0; i < 5; i++) {
		send_state_machine_bit(1);
	}
}

void jtag_prog_set_delay(int d) {
	delay = d;
}

void jtag_prog_init(void) {
	ioport_set_pin_dir(A_TDI, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_TDO, IOPORT_DIR_INPUT);
	ioport_set_pin_dir(A_TMS, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(A_TCK, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(A_TDI, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_TMS, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_TCK, IOPORT_PIN_LEVEL_LOW);
	avr_prog_set_reset_high_voltage(false);
	jtag_reset();
	send_state_machine_bit(0);
}

static bool set_TDI(bool c) {
	if (c) {
		ioport_set_pin_level(A_TDI, IOPORT_PIN_LEVEL_HIGH);
	} else {
		ioport_set_pin_level(A_TDI, IOPORT_PIN_LEVEL_LOW);
	}
	return ioport_get_pin_level(A_TDO);
}

static void send_command(char command) {
	//Shift-IR
	send_state_machine_bit(1);
	send_state_machine_bit(1);
	send_state_machine_bit(0);
	send_state_machine_bit(0);


	set_TDI((command & 0x01) != 0);
	send_state_machine_bit(0);
	set_TDI((command & 0x02) != 0);
	send_state_machine_bit(0);
	set_TDI((command & 0x04) != 0);
	send_state_machine_bit(0);
	set_TDI((command & 0x08) != 0);
	send_state_machine_bit(1);
	send_state_machine_bit(1);
	send_state_machine_bit(0);
}

int jtag_prog_detect_number_of_devices(void) {
	jtag_reset();
	send_state_machine_bit(0);
	//Shift IR
	send_state_machine_bit(1);
	send_state_machine_bit(1);
	send_state_machine_bit(0);
	send_state_machine_bit(0);
	// Put all devices into bypass (hopefully!)
	int i;
	for (i = 0; i < 999; i++) {
		set_TDI(1);
		send_state_machine_bit(0);
	}
	set_TDI(1);
	// Idle
	send_state_machine_bit(1);
	send_state_machine_bit(1);
	send_state_machine_bit(0);
	// Shift DR
	send_state_machine_bit(1);
	send_state_machine_bit(0);
	send_state_machine_bit(0);
	// Flush with zeroes
	for (i = 0; i < 1000; i++) {
		set_TDI(0);
		send_state_machine_bit(0);
	}
	// Send ones and count
	for (i = 0; i < 1000; i++) {
		if (set_TDI(1)) {
			break;
		}
		send_state_machine_bit(0);
	}
	// Idle
	send_state_machine_bit(1);
	send_state_machine_bit(1);
	send_state_machine_bit(0);
	return i;
}

static uint32_t read_int32(void) {
	uint32_t r = 0;
	for (int i = 0; i < 32; i++) {
		r |= (set_TDI(0) << i);
		send_state_machine_bit(0);
	}
	return r;
}

uint32_t jtag_prog_read_id_code(int deviceNo) {
	jtag_reset();
	//Shift DR
	send_state_machine_bit(0);
	send_state_machine_bit(1);
	send_state_machine_bit(0);
	send_state_machine_bit(0);
	uint32_t r;
	for (int d = 0; d <= deviceNo; d++) {
		r = read_int32();
	}
	// Idle
	send_state_machine_bit(1);
	send_state_machine_bit(1);
	send_state_machine_bit(0);
	return r;
}