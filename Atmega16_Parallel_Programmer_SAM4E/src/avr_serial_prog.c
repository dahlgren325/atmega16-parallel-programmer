/*
 * avr_serial_prog.c
 *
 * Created: 2016-11-28 01:08:00
 *  Author: Ronnie
 */

#include "avr_prog.h"
#include "ioport.h"
#include <stdlib.h>
#include <delay.h>
#include "avr_serial_prog.h"

#define MOSI A_PB5
#define MISO A_PB6
#define SCK A_PB7

static char *buffer = 0;
static int bufferSize = 0;

RAMFUNC
static void wait_loop(unsigned int val) {
	UNUSED(val);

	__asm (
	"loop: DMB	\n"
	"SUBS R0, R0, #1  \n"
	"BNE.N loop         "
	);
}

OPTIMIZE_HIGH
static void wait(unsigned int val) {
	wait_loop(val);
}

static void pulseSck(void) {
	wait(50);
	ioport_set_pin_level(SCK, IOPORT_PIN_LEVEL_HIGH);
	wait(50);
	//bool r = ioport_get_pin_level(MISO);
	ioport_set_pin_level(SCK, IOPORT_PIN_LEVEL_LOW);
	//return r;
}

static char transferOne(unsigned char byte) {
	char input = 0;
	char a = byte;
	for (int i = 0; i < 8; i++) {
		input <<= 1;
		if ((a & 0x80) != 0) {
			ioport_set_pin_level(MOSI, IOPORT_PIN_LEVEL_HIGH);
		} else {
			ioport_set_pin_level(MOSI, IOPORT_PIN_LEVEL_LOW);
		}
		if (ioport_get_pin_level(MISO)) {
			input |= 0x01;
		}
		pulseSck();
		a <<= 1;
	}
	wait(50);
	return input;
}

static bool avr_serial_prog_enable(void) {
	transferOne(0xac);
	transferOne(0x53);
	char response = transferOne(0x00);
	transferOne(0x00);

	if (response != 0x53) {
		ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_HIGH);
		wait(20000);
		ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_LOW);
		wait(20000);
		transferOne(0xac);
		transferOne(0x53);
		response = transferOne(0x00);
		transferOne(0x00);
		return response == 0x53;
	}
	return true;
}

bool avr_serial_prog_init(void) {
	REG_PIOE_PER |= PIO_PER_P2;
	REG_PIOE_PUDR |= PIO_PUDR_P2;
	ioport_set_pin_dir(MOSI, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(MISO, IOPORT_DIR_INPUT);
	ioport_set_pin_dir(SCK, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(MOSI, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(SCK, IOPORT_PIN_LEVEL_LOW);
	avr_prog_set_reset_high_voltage(false);
	wait(200000);
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_HIGH);
	wait(200000);
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_LOW);
	ioport_set_pin_level(A_POWER, IOPORT_PIN_LEVEL_LOW);
	wait(200000);
	ioport_set_pin_level(A_POWER, IOPORT_PIN_LEVEL_HIGH);
	wait(200000);
	return avr_serial_prog_enable();
}

void avr_serial_prog_set_buffer(char *buf, int size) {
	avr_prog_free_buffer();
	if (size != 0) {
		buffer = buf;
		bufferSize = size;
	}
}

void avr_serial_prog_free_buffer(void) {
	if (bufferSize != 0) {
		free(buffer);
		bufferSize = 0;
	}
}

void avr_serial_prog_write_buffer_flash(int address) {
	if (bufferSize == 0) {
		return;
	}
	int bufferOffset = 0;
	while (bufferOffset < bufferSize) {
		avr_serial_prog_write_word(address + bufferOffset / 2, (int)((buffer[bufferOffset + 1] << 8)) | ((int)buffer[bufferOffset]));
		bufferOffset += 2;
	}
	avr_serial_prog_write_page(address);
}

void avr_serial_prog_write_word(int address, int data) {
	// Page address low byte
	transferOne(0x40);
	transferOne((address & 0x0000) >> 8);
	transferOne(address & 0x003f);
	transferOne(data & 0x00ff);

	// Page address high byte
	transferOne(0x48);
	transferOne((address & 0x0000) >> 8);
	transferOne(address & 0x003f);
	transferOne((data & 0x0ff00) >> 8);
}

void avr_serial_prog_end(void) {
	ioport_set_pin_level(A_RESET, IOPORT_PIN_LEVEL_HIGH);
}

void avr_serial_prog_write_low_fuse(char fuse) {
	transferOne(0xac);
	transferOne(0xa0);
	transferOne(0x00);
	transferOne(fuse);
}

void avr_serial_prog_write_high_fuse(char fuse) {
	transferOne(0xac);
	transferOne(0xa8);
	transferOne(0x00);
	transferOne(fuse);
}

void avr_serial_prog_write_lock_bits(char lockbits) {
	transferOne(0xac);
	transferOne(0xe0);
	transferOne(0x00);
	transferOne(lockbits);
}

char avr_serial_prog_read_signature(char address) {
	transferOne(0x30);
	transferOne(0x00);
	transferOne(address & 0x03);
	return transferOne(0);
}

char avr_serial_prog_read_low_fuse(void) {
	transferOne(0x50);
	transferOne(0x00);
	transferOne(0x00);
	return transferOne(0);
}

char avr_serial_prog_read_high_fuse(void) {
	transferOne(0x58);
	transferOne(0x08);
	transferOne(0x00);
	return transferOne(0);
}

char avr_serial_prog_read_lock_bits(void) {
	transferOne(0x58);
	transferOne(0x00);
	transferOne(0x00);
	return transferOne(0);
}

void avr_serial_prog_chiperase(void) {
	transferOne(0xac);
	transferOne(0x80);
	transferOne(0x00);
	transferOne(0);
}

uint16_t avr_serial_prog_read_flash(int address) {
	transferOne(0x28);
	transferOne(address >> 8);
	transferOne(address & 0x00ff);
	char h = transferOne(0);
	transferOne(0x20);
	transferOne(address >> 8);
	transferOne(address & 0x00ff);
	char l = transferOne(0);
	return (l << 8) | h;
}

void avr_serial_prog_write_page(int address) {
	transferOne(0x4c);
	transferOne((address & 0b0001111100000000) >> 8);
	transferOne(address & 0b011000000);
	transferOne(0x00);
	wait(40000);
}
