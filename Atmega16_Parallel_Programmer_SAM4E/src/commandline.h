/*
 * commandline.h
 *
 * Created: 2016-11-19 10:32:24
 *  Author: Ronnie
 */ 


#ifndef COMMANDLINE_H_
#define COMMANDLINE_H_

void commandline_parse(char *commandline, int max);
void commandline_output(const char *msg);
void commandline_flushout(void);

#endif /* COMMANDLINE_H_ */