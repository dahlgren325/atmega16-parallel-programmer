﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace ProgrammerWindowsApp
{
    class ReadBsdlException : Exception
    {
        public ReadBsdlException()
        {
        }

        public ReadBsdlException(string message) : base(message)
        {
        }

        public ReadBsdlException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ReadBsdlException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    public class PinMap
    {
        public string Name { get; set; }
        public Dictionary<string, PinMapEntry> Entries { get; set; }
    }

    public class PinMapEntry
    {
        public string Signal { get; set; }
        public List<int> Pins { get; set; }
    }

    public enum BoundaryRegistryEntityDefaultValue
    {
        DontCare, Zero, One
    }

    public class BoundaryRegisterEntry
    {
        public int CellNumber { get; set; }
        public string CellType { get; set; }
        public string PortId { get; set; }
        public string OutputType { get; set; }
        public BoundaryRegistryEntityDefaultValue DefaultValue { get; set; }
        public int DisableOutput { get; set; } = -1;
        public int DisableOutputValue { get; set; } = -1;
        public string OutputDriverDisabledCondition { get; set; }
    }

    public class InstructionOpcode
    {
        public string Name { get; set; }
        public int Opcode;
    }

    public class BsdlDescription
    {
        public List<PortDescription> PortDescriptions { get; set; }
        public string ScanTdi { get; set; }
        public string ScanTdo { get; set; }
        public string ScanTck { get; set; }
        public string ScanTms { get; set; }
        public string PackageType { get; set; }
        public int InstructionLength { get; set; }
        public Dictionary<string, PinMap> PinMaps { get; set; } = new Dictionary<string, PinMap>();
        public Dictionary<string, InstructionOpcode> InstructionOpcodes { get; set; } = new Dictionary<string, InstructionOpcode>();
        public List<string> InstructionPrivates { get; set; } = new List<string>();
        public int InstructionCapture { get; set; }
        public Int32 IdCodeRegister { get; set; }
        public int BoundaryLength { get; set; }
        public Dictionary<string, List<string>> RegisterAccess { get; set; } = new Dictionary<string, List<string>>();
        public List<BoundaryRegisterEntry> BoundaryRegister { get; set; } = new List<BoundaryRegisterEntry>(); 
    }

    public class PortDescription
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class ReadBsdl
    {
        public BsdlDescription Parse(string file)
        {
            var bsdlDescription = new BsdlDescription();
            IEnumerable<string> lines = File.ReadAllLines(file);
            lines = lines.Where(l => !l.Trim().StartsWith("--") && !string.IsNullOrEmpty(l.Trim()));
            string txt = string.Join("", lines);
            var beginRx = new Regex("\\bentity\\s+(\\w*)\\s+is\\b");
            var match = beginRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException("'entity <label> is' expected");
            var entityDescription = match.Groups[1].Value;
            txt = txt.Substring(match.Index + match.Length).Trim();
            var endRx = new Regex($"\\bend\\s+{entityDescription}\\s*;");
            match = endRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException("'end <label>;' expected");
            txt = txt.Substring(0, match.Index).Trim();

            var genericRx = new Regex("\\bgeneric\\s*\\(PHYSICAL_PIN_MAP\\s*:\\s*string\\s*:=\\s*\"\\s*(\\w+)\\s*\"\\)\\s*;");
            match = genericRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException("'generic (PHYSICAL_PIN_MAP:string:=\"<package type>\");' expected");
            var packageType = match.Groups[1].Value;
            bsdlDescription.PackageType = packageType;
            txt = txt.Substring(match.Index + match.Length).Trim();

            var portStartRx = new Regex("\\bport\\s*\\(");
            match = portStartRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException("'port (' expected");
            txt = txt.Substring(match.Index + match.Length).Trim();

            int i;
            bsdlDescription.PortDescriptions = ParsePortDescription(txt, out i);
            txt = txt.Substring(i).Trim();

            var pinMapRx = new Regex("\\bconstant\\s+(\\w+)\\s*:\\s*PIN_MAP_STRING\\s*:=\\s*(\".*?\")\\s*;");
            match = pinMapRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException("'constant <package>:PIN_MAP_STRING:=' expected");
            var pinMapName = match.Groups[1].Value;
            var pinMapString = string.Join("", match.Groups[2].Value.Split('&').Select(l => l.Trim(' ', '"')));
            var pinMap = ParsePinMapString(pinMapString);
            pinMap.Name = pinMapName;
            bsdlDescription.PinMaps.Add(pinMap.Name, pinMap);
            txt = txt.Substring(match.Index + match.Length).Trim();

            while (true)
            {
                match = pinMapRx.Match(txt);
                if (!match.Success)
                    break;
                var pinMapName2 = match.Groups[1].Value;
                var pinMapString2 = string.Join("", match.Groups[2].Value.Split('&').Select(l => l.Trim(' ', '"')));
                var pinMap2 = ParsePinMapString(pinMapString2);
                pinMap2.Name = pinMapName2;
                bsdlDescription.PinMaps.Add(pinMap2.Name, pinMap2);
                txt = txt.Substring(match.Index + match.Length).Trim();
            }

            var tdiRx = new Regex("\\battribute\\s+TAP_SCAN_IN\\s+of\\s+(\\w+)\\s*:\\s*signal\\s+is\\s+(\\w+)\\s*;");
            var tdoRx = new Regex("\\battribute\\s+TAP_SCAN_OUT\\s+of\\s+(\\w+)\\s*:\\s*signal\\s+is\\s+(\\w+)\\s*;");
            var tmsRx = new Regex("\\battribute\\s+TAP_SCAN_MODE\\s+of\\s+(\\w+)\\s*:\\s*signal\\s+is\\s+(\\w+)\\s*;");
            var tckRx = new Regex("\\battribute\\s+TAP_SCAN_CLOCK\\s+of\\s+(\\w+)\\s*:\\s*signal\\s+is\\s+([^;]+);");
            var matchTdi = tdiRx.Match(txt);
            var matchTdo = tdoRx.Match(txt);
            var matchTms = tmsRx.Match(txt);
            var matchTck = tckRx.Match(txt);
            if (matchTdi.Success)
                bsdlDescription.ScanTdi = matchTdi.Groups[1].Value;
            if (matchTdo.Success)
                bsdlDescription.ScanTdo = matchTdo.Groups[1].Value;
            if (matchTms.Success)
                bsdlDescription.ScanTms = matchTms.Groups[1].Value;
            if (matchTck.Success)
                bsdlDescription.ScanTck = matchTck.Groups[1].Value;
            var instructionLengthRx = new Regex($"\\battribute\\s+INSTRUCTION_LENGTH\\s+of\\s+{entityDescription}\\s*:\\s*entity\\s+is\\s+(\\d+)\\s*;");
            match = instructionLengthRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException($"'attribute INSTRUCTION_LENGTH of {entityDescription}: entity is <size>;' expected");
            bsdlDescription.InstructionLength = Convert.ToInt32(match.Groups[1].Value);

            var instructionOpcodeRx = new Regex($"\\battribute\\s+INSTRUCTION_OPCODE\\s+of\\s+{entityDescription}\\s*:\\s*entity\\s+is\\s+\"(.*?)\"\\s*;");
            match = instructionOpcodeRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException($"'attribute INSTRUCTION_OPCODE' expected");
            var instructionOpcodeString = match.Groups[1].Value.Trim();
            bsdlDescription.InstructionOpcodes = ParseInstructionOpcode(instructionOpcodeString);

            var instructionPrivateRx = new Regex($"\\battribute\\s+INSTRUCTION_PRIVATE\\s+of\\s+{entityDescription}\\s*:\\s*entity\\s+is\\s+\"(.*?)\"\\s*;");
            match = instructionPrivateRx.Match(txt);
            if (match.Success)
            {
                var instructionPrivateString = match.Groups[1].Value.Trim();
                bsdlDescription.InstructionPrivates = ParseInstructionPrivate(instructionPrivateString);
            }

            var instructionCaptureRx = new Regex($"\\battribute\\s+INSTRUCTION_CAPTURE\\s+of\\s+{entityDescription}\\s*:\\s*entity\\s+is\\s+\"(.*?)\"\\s*;");
            match = instructionCaptureRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException($"'attribute INSTRUCTION_CAPTURE' expected");
            bsdlDescription.InstructionCapture = Convert.ToInt32(match.Groups[1].Value.Trim(), 2);

            var idCodeRegisterRx = new Regex($"\\battribute\\s+IDCODE_REGISTER\\s+of\\s+{entityDescription}\\s*:\\s*entity\\s+is\\s+\"(.*?)\"\\s*;");
            match = idCodeRegisterRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException($"'attribute IDCODE_REGISTER' expected");
            var joinRx = new Regex("\"\\s*&\\s*\"");
            var idCodeRegisterString = joinRx.Replace(match.Groups[1].Value, "");
            bsdlDescription.IdCodeRegister = Convert.ToInt32(idCodeRegisterString, 2);

            var boundaryLengthRx = new Regex($"\\battribute\\s+BOUNDARY_LENGTH\\s+of\\s+{entityDescription}\\s*:\\s*entity\\s+is\\s+(\\d+?)\\s*;");
            match = boundaryLengthRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException($"'attribute BOUNDARY_LENGTH' expected");
            bsdlDescription.BoundaryLength = Convert.ToInt32(match.Groups[1].Value);

            var registerAccessRx = new Regex($"\\battribute\\s+REGISTER_ACCESS\\s+of\\s+{entityDescription}\\s*:\\s*entity\\s+is\\s+\"(.+?)\"\\s*;");
            match = registerAccessRx.Match(txt);
            if (match.Success)
            {
                var joined = joinRx.Replace(match.Groups[1].Value, "");
                var split = SplitPinMapString(joined);
                var registerAccessRx2 = new Regex($"\\s*(\\w+)\\s*\\((?:\\s*(\\w+)\\s*,)*\\s*(\\w+)\\s*\\)");
                foreach (var v in split)
                {
                    match = registerAccessRx2.Match(v);
                    if (!match.Success)
                        throw new ReadBsdlException($"Wrong register access entry: {v}");
                    if (!bsdlDescription.RegisterAccess.ContainsKey(match.Groups[1].Value))
                        bsdlDescription.RegisterAccess.Add(match.Groups[1].Value, new List<string>());
                    var registerAccess = bsdlDescription.RegisterAccess[match.Groups[1].Value];
                    registerAccess.AddRange(from Capture capture in match.Groups[2].Captures select capture.Value);
                    registerAccess.Add(match.Groups[3].Value);
                }
            }

            var boundaryRegisterRx = new Regex($"\\battribute\\s+BOUNDARY_REGISTER\\s+of\\s+{entityDescription}\\s*:\\s*entity\\s+is\\s+\"(.+?)\"\\s*;");
            match = boundaryRegisterRx.Match(txt);
            if (!match.Success)
                throw new ReadBsdlException($"'attribute BOUNDARY_REGISTER' expected");
            var joined2 = joinRx.Replace(match.Groups[1].Value, "");
            foreach (var boundaryRegisterEntryString in SplitPinMapString(joined2))
            {
                var boundaryRegisterEntryRx = new Regex($"\\s*(\\d+)\\s*\\(\\s*(\\w+)\\s*,\\s*(.+?)\\s*,\\s*(\\w+)\\s*,\\s*(.+?)\\s*\\)");
                match = boundaryRegisterEntryRx.Match(boundaryRegisterEntryString);
                if (!match.Success)
                    throw new ReadBsdlException($"Wrong boundary register entry: {boundaryRegisterEntryString}");
                var cellNumber = Convert.ToInt32(match.Groups[1].Value);
                var cellType = match.Groups[2].Value;
                var portId = match.Groups[3].Value;
                var outputType = match.Groups[4].Value;
                var boundaryRegisterEntry = new BoundaryRegisterEntry
                {
                    CellNumber = cellNumber,
                    CellType = cellType,
                    PortId = portId,
                    OutputType = outputType,
                };
                var rest = match.Groups[5].Value;
                var boundaryRegisterEntryRestRx = new Regex($"\\s*([X01])\\s*,\\s*(\\d+)\\s*,\\s*([01])\\s*,\\s*([Z01])");
                match = boundaryRegisterEntryRestRx.Match(rest);
                if (match.Success)
                {
                    var defaultValue = match.Groups[1].Value;
                    var disableOutput = Convert.ToInt32(match.Groups[2].Value);
                    var disableOutputValue = Convert.ToInt32(match.Groups[3].Value);
                    var condition = match.Groups[4].Value;
                    boundaryRegisterEntry.DefaultValue = defaultValue == "X"
                        ? BoundaryRegistryEntityDefaultValue.DontCare
                        : defaultValue == "0"
                            ? BoundaryRegistryEntityDefaultValue.Zero
                            : BoundaryRegistryEntityDefaultValue.One;
                    boundaryRegisterEntry.DisableOutput = disableOutput;
                    boundaryRegisterEntry.DisableOutputValue = disableOutputValue;
                    boundaryRegisterEntry.OutputDriverDisabledCondition = condition;
                }
                else
                {
                    boundaryRegisterEntry.DefaultValue = rest == "X"
                        ? BoundaryRegistryEntityDefaultValue.DontCare
                        : rest == "0"
                            ? BoundaryRegistryEntityDefaultValue.Zero
                            : BoundaryRegistryEntityDefaultValue.One;
                }
                bsdlDescription.BoundaryRegister.Add(boundaryRegisterEntry);
            }
            int a = 0;


            return bsdlDescription;
        }

        private List<string> ParseInstructionPrivate(string txt)
        {
            var result = new List<string>();
            var joinRx = new Regex("\"\\s*&\\s*\"");
            txt = joinRx.Replace(txt, "");
            foreach (var entry in txt.Split(','))
            {
                var opcodeRx = new Regex("\\s*(\\w+)\\s*");
                var match = opcodeRx.Match(entry);
                if (!match.Success)
                    throw new ReadBsdlException("Invalid instruction entry: " + entry);
                result.Add(match.Groups[1].Value);
            }
            return result;
        }

        private Dictionary<string, InstructionOpcode> ParseInstructionOpcode(string txt)
        {
            var result = new Dictionary<string, InstructionOpcode>();
            var joinRx = new Regex("\"\\s*&\\s*\"");
            txt = joinRx.Replace(txt, "");
            foreach (var entry in SplitPinMapString(txt))
            {
                var opcodeRx = new Regex("\\s*(\\w+)\\s*\\(\\s*(\\d+)\\s*\\)");
                var match = opcodeRx.Match(entry);
                if (!match.Success)
                    throw new ReadBsdlException("Invalid opcode entry: " + entry);
                var instructionOpcode = new InstructionOpcode
                {
                    Name = match.Groups[1].Value,
                    Opcode = Convert.ToInt32(match.Groups[2].Value, 2)
                };
                result.Add(instructionOpcode.Name, instructionOpcode);
            }
            return result;
        }

        private PinMap ParsePinMapString(string txt)
        {
            var result = new PinMap
            {
                Entries = new Dictionary<string, PinMapEntry>()
            };
            txt = txt.Trim();
            var joinRx = new Regex("\"\\s*&\\s*\"");
            txt = joinRx.Replace(txt, "");
            txt = txt.Trim('"');
            foreach (var entry1 in SplitPinMapString(txt))
            {
                var entry2 = entry1.Trim();
                var strings = entry2.Split(':');
                var signal = strings[0].Trim();
                var pins = strings[1].Trim();
                var pinRx = new Regex("\\((?:(\\d+),)*(\\d+)\\)");
                var match = pinRx.Match(pins);
                var pinMapEntry = new PinMapEntry
                {
                    Signal = signal,
                    Pins = new List<int>()
                };
                if (match.Success)
                {
                    foreach (Capture capture in match.Groups[1].Captures)
                    {
                        pinMapEntry.Pins.Add(Convert.ToInt32(capture.Value));
                    }
                    pinMapEntry.Pins.Add(Convert.ToInt32(match.Groups[2].Value));
                }
                else
                {
                    pinMapEntry.Pins.Add(Convert.ToInt32(pins));
                }
                result.Entries.Add(signal, pinMapEntry);
            }
            return result;
        }

        private List<string> SplitPinMapString(string txt)
        {
            int pCount = 0;
            var result = new List<string>();
            string value = "";
            foreach (char c in txt)
            {
                if (c == ')')
                {
                    if (pCount == 0)
                        throw new ReadBsdlException("Unmatched ')'");
                    pCount--;
                    value += c;
                    continue;
                }
                if (c == '(')
                {
                    pCount++;
                    value += c;
                    continue;
                }
                if (c == ',')
                {
                    if (pCount != 0)
                    {
                        value += c;
                        continue;
                    }
                    result.Add(value.Trim());
                    value = "";
                    continue;
                }
                value += c;
            }
            if (!string.IsNullOrEmpty(value.Trim()))
            {
                result.Add(value.Trim());
            }
            return result;
        }

        private List<PortDescription> ParsePortDescription(string txt, out int i)
        {
            List<PortDescription> result = new List<PortDescription>();
            int state = 0;
            i = 0;
            int leftParanthesesCount = 0;
            var currentPortDescription = new PortDescription();
            while (true)
            {
                char c = txt[i++];
                if (state == 0)
                {
                    if (c == ':')
                        throw new ReadBsdlException("Invalid port description");
                    if (c != ' ')
                    {
                        currentPortDescription.Name = "" + c;
                        state = 1;
                    }
                    continue;
                }
                if (state == 1)
                {
                    if (c == ':')
                    {
                        state = 2;
                        currentPortDescription.Name = currentPortDescription.Name.Trim();
                    }
                    else
                        currentPortDescription.Name += c;
                    continue;
                }
                if (state == 2)
                {
                    if (c == ' ')
                        state = 4;
                    else if (c == ':')
                        throw new ReadBsdlException("Invalid port description");
                    else if (c == ';')
                        throw new ReadBsdlException("Invalid port description");
                    continue;
                }
                if (state == 4)
                {
                    if (c != ' ')
                    {
                        currentPortDescription.Type = "" + c;
                        state = 5;
                    }
                    else if (c == ':')
                        throw new ReadBsdlException("Invalid port description");
                    else if (c == ';')
                        throw new ReadBsdlException("Invalid port description");
                    continue;
                }
                if (state == 5)
                {
                    if (c == ';')
                    {
                        state = 0;
                        currentPortDescription.Type = currentPortDescription.Type.Trim();
                        result.Add(currentPortDescription);
                        currentPortDescription = new PortDescription();
                    }
                    else if (c == '(')
                    {
                        leftParanthesesCount++;
                        currentPortDescription.Type += c;
                    }
                    else if (c == ')' && leftParanthesesCount > 0)
                    {
                        leftParanthesesCount--;
                        currentPortDescription.Type += c;
                    }
                    else if (c == ')')
                        state = 6;
                    else
                        currentPortDescription.Type += c;
                    continue;
                }
                if (state == 6)
                {
                    if (c == ';')
                    {
                        currentPortDescription.Type = currentPortDescription.Type.Trim();
                        result.Add(currentPortDescription);
                        return result;
                    }
                    if (c != ' ')
                        throw new ReadBsdlException("Invalid port description");
                }
            }
        }
    }
}
