﻿using System;
using System.Runtime.Serialization;

namespace ProgrammerWindowsApp
{
    public class ReadElfException: Exception
    {
        public ReadElfException()
        {
        }

        public ReadElfException(string message) : base(message)
        {
        }

        public ReadElfException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ReadElfException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}