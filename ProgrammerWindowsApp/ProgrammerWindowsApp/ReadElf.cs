﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using D = ProgrammerWindowsApp.Definitions;

namespace ProgrammerWindowsApp
{
    public class ReadElf
    {
        private readonly byte[] _bytes;
        private int _offset;
        private readonly List<byte> _memory = new List<byte>();

        public List<byte> Memory => _memory;

        private byte ReadByte(int address)
        {
            address += _offset;
            return _bytes[address];
        }

        public ReadElf(byte[] bytes)
        {
            _bytes = bytes;
        }

        private int ReadWord(int address)
        {
            address += _offset;
            return (_bytes[address + 1] << 8) + _bytes[address];
        }

        private int ReadLong(int address)
        {
            address += _offset;
            return (_bytes[address + 3] << 24) + (_bytes[address + 2] << 16) + (_bytes[address + 1] << 8) + _bytes[address];
        }

        public void ReadFile()
        {
            if (!_bytes.Take(4).SequenceEqual(D.EI_MAG))
            {
                throw new ReadElfException("Not an elf file");
            }

            var ei_class = ReadByte(4);
            var e_type = ReadWord(0x10);
            var e_machine = ReadWord(0x12);

            if (ei_class != 1)
            {
                throw new ReadElfException("Only 32 bit elf file supported");
            }

            if (e_type != 2)
            {
                throw new ReadElfException("Not an executable elf file");
            }

            if (e_machine != D.EM_AVR)
            {
                throw new ReadElfException("Not an AVR elf file");
            }

            var e_phoff = ReadLong(0x1c);
            var e_phentsize = ReadWord(0x2A);
            var e_phnum = ReadWord(0x2C);

            for (int i = 0; i < e_phnum; i++)
            {
                ReadProgramHeader(e_phoff + i * e_phentsize);
            }
            var str = string.Join(" ", _memory.Select(v => v.ToString("X2")));
            int a = 0;
        }

        private void ReadProgramHeader(int address)
        {
            _offset = address;
            var p_type = ReadLong(0x00);
            var p_offset = ReadLong(0x04);
            var p_vaddr = ReadLong(0x08);
            var p_paddr = ReadLong(0x0C);
            var p_filesz = ReadLong(0x10);
            var p_memsz = ReadLong(0x14);
            var p_align = ReadLong(0x1C);
            if (p_type == D.PT_LOAD)
            {
                LoadIntoMem(p_offset, p_vaddr, p_filesz);
            }
        }

        private void PutInMem(int address, byte value)
        {
            while (_memory.Count <= address)
                _memory.Add(0);
            _memory[address] = value;
        }

        private void LoadIntoMem(int fileAddress, int memAddress, int size)
        {
            _offset = fileAddress;
            for (int i = 0; i < size; i++)
                PutInMem(memAddress++, _bytes[fileAddress++]);
        }
    }
}