﻿namespace ProgrammerWindowsApp
{
    public static class Definitions
    {
        public static byte[] EI_MAG = { 0x7f, 0x45, 0x4c, 0x46 };
        public static int EM_AVR = 83;
        public static int PT_LOAD = 0x01;
    }
}
