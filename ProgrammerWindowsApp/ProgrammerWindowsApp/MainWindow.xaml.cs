﻿using System;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using ProgrammerWindowsApp.LexicalParser;

namespace ProgrammerWindowsApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private SerialPort _serialPort;
        private readonly Semaphore _readOperationSemaphore = new Semaphore(0, 1);
        private readonly MemoryStream _flashBuffer = new MemoryStream();
        private bool _readOperationUnderway;
        private bool _writeOperationUnderway;

        private readonly object _connectionLifeCycleLock = new object();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SelectFileButton_OnClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            var showDialog = openFileDialog.ShowDialog(this);
            if (showDialog == false)
                return;
            SelectedFileTextBox.Text = openFileDialog.FileName;
        }

        private void Output(string text, bool withLineBreak = true)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() =>
                {
                    OutputTextBox.Text += text + (withLineBreak ? "\n" : "");
                    OutputTextBox.ScrollToEnd();
                });
            }
            else
            {
                OutputTextBox.Text += text + (withLineBreak ? "\n" : "");
                OutputTextBox.ScrollToEnd();
            }
        }

        private void ProgramButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            var file = SelectedFileTextBox.Text;
            var bytes = File.ReadAllBytes(file);
            var readElf = new ReadElf(bytes);
            readElf.ReadFile();
            if (readElf.Memory.Count == 0)
            {
                Output("Nothing to program");
                return;
            }
            _writeOperationUnderway = true;
            new Thread(() =>
            {
                try
                {
                    for (int i = 0; i < readElf.Memory.Count; i += 128)
                    {
                        var b64 = Convert.ToBase64String(readElf.Memory.Skip(i).Take(128).ToArray());
                        SendData("trans " + b64);
                        _readOperationSemaphore.WaitOne(2000);
                        SendData("write " + i / 2);
                        _readOperationSemaphore.WaitOne(2000);
                    }
                }
                finally
                {
                    _writeOperationUnderway = false;
                }
            }).Start();
        }

        private void ReadFuseButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("readfuse");
        }

        private void InitButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("init");
        }

        private void ConnectButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort != null && _serialPort.IsOpen)
            {
                Output("Already connected");
                return;
            }
            _serialPort = new SerialPort
            {
                PortName = ComPortCombo.Text,
                BaudRate = 9600,
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                Handshake = Handshake.None,
                ReadTimeout = 3000,
                WriteTimeout = 3000

            };
            var buffer = new byte[100000];
            Action kickOffRead = null;
            kickOffRead = () =>
            {
                if (_serialPort == null)
                    return;
                try
                {
                    _serialPort.BaseStream.BeginRead(buffer, 0, buffer.Length, ar =>
                    {
                        string txt;
                        try
                        {
                            var actualLength = _serialPort.BaseStream.EndRead(ar);
                            var received = new byte[actualLength];
                            Buffer.BlockCopy(buffer, 0, received, 0, actualLength);
                            txt = Encoding.UTF8.GetString(received);
                            if (!Dispatcher.CheckAccess())
                            {
                                Dispatcher.Invoke(() =>
                                {
                                    DoLogic(txt);
                                });

                            }
                            else
                            {
                                DoLogic(txt);
                            }
                            Output(txt.Replace("\n\r\n", "\n"), false);
                            if ((_readOperationUnderway || _writeOperationUnderway) && txt.Contains(">"))
                            {
                                _readOperationSemaphore.Release();
                            }
                        }
                        catch (Exception ex)
                        {
                            SerialPort sp;
                            lock (_connectionLifeCycleLock)
                            {
                                sp = _serialPort;
                            }
                            if (sp == null || !sp.IsOpen)
                                return;
                            if (!Dispatcher.CheckAccess())
                            {
                                Dispatcher.Invoke(() =>
                                {
                                    MessageBox.Show(ex.Message);
                                });

                            }
                            else
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                        finally
                        {
                            kickOffRead();
                        }
                    }, null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            };
            try
            {
                _serialPort.Open();
                kickOffRead();
                Output("Connected!");
            }
            catch (UnauthorizedAccessException ex)
            {
                Output("Could not connect - " + ex.Message);
            }
            catch (IOException ex)
            {
                Output("Could not connect - " + ex.Message);
            }
        }

        private void DoLogic(string msg)
        {
            HandleFuseMessage(msg);
            HandleFlashMessage(msg);

        }

        private void HandleFlashMessage(string msg)
        {
            var fuseRegex = new Regex(@"\bflash ([0-9A-Za-z+/=]+)");
            var match = fuseRegex.Match(msg);
            if (!match.Success)
                return;
            var bytes = Convert.FromBase64String(match.Groups[1].Value);
            _flashBuffer.Write(bytes, 0, bytes.Length);
        }

        private void HandleFuseMessage(string msg)
        {
            var fuseRegex = new Regex("<lock><highfuse><lowfuse>: ([0-9A-Za-z]{6})");
            var match = fuseRegex.Match(msg);
            if (!match.Success)
                return;
            var str = match.Groups[1].Value;
            int lck = Convert.ToInt32(str.Substring(0, 2), 16);
            int high = Convert.ToInt32(str.Substring(2, 2), 16);
            int low = Convert.ToInt32(str.Substring(4, 2), 16);
            LB1CheckBox.IsChecked = (lck & 0x01) == 0;
            LB2CheckBox.IsChecked = (lck & 0x02) == 0;
            BLB01CheckBox.IsChecked = (lck & 0x04) == 0;
            BLB02CheckBox.IsChecked = (lck & 0x08) == 0;
            BLB11CheckBox.IsChecked = (lck & 0x10) == 0;
            BLB12CheckBox.IsChecked = (lck & 0x20) == 0;
            CKSEL0CheckBox.IsChecked = (low & 0x01) == 0;
            CKSEL1CheckBox.IsChecked = (low & 0x02) == 0;
            CKSEL2CheckBox.IsChecked = (low & 0x04) == 0;
            CKSEL3CheckBox.IsChecked = (low & 0x08) == 0;
            SUT0CheckBox.IsChecked = (low & 0x10) == 0;
            SUT1CheckBox.IsChecked = (low & 0x20) == 0;
            BODENCheckBox.IsChecked = (low & 0x40) == 0;
            BODLEVELCheckBox.IsChecked = (low & 0x80) == 0;
            BOOTRSTCheckBox.IsChecked = (high & 0x01) == 0;
            BOOTSZ0CheckBox.IsChecked = (high & 0x02) == 0;
            BOOTSZ1CheckBox.IsChecked = (high & 0x04) == 0;
            EESAVECheckBox.IsChecked = (high & 0x08) == 0;
            CKOPTCheckBox.IsChecked = (high & 0x10) == 0;
            SPIENCheckBox.IsChecked = (high & 0x20) == 0;
            JTAGENCheckBox.IsChecked = (high & 0x40) == 0;
            OCDENCheckBox.IsChecked = (high & 0x80) == 0;
        }

        private void SendData(string msg)
        {
            try
            {
                _serialPort.WriteLine(msg);
            }
            catch (Exception e)
            {
                Output(e.Message);
            }
        }

        private void ClearButton_OnClick(object sender, RoutedEventArgs e)
        {
            OutputTextBox.Clear();
        }

        private void DisconnectButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null || !_serialPort.IsOpen)
            {
                Output("Not connected");
                return;
            }
            lock (_connectionLifeCycleLock)
            {
                try
                {
                    _serialPort.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                _serialPort = null;
            }
            Output("Disconnected");
        }

        private void ReadSignatureButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("init");
            SendData("sig");
        }

        private void ReadFlashButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            _flashBuffer.Seek(0, SeekOrigin.Begin);
            _flashBuffer.SetLength(0);
            _readOperationUnderway = true;
            new Thread(() =>
            {
                try
                {
                    //for (int page = 0; page < 20; page++)
                    for (int page = 0; page < 128; page++)
                    {
                        int address = page * 64;
                        SendData("read " + address + " 64");
                        _readOperationSemaphore.WaitOne(2000);
                    }
                }
                finally
                {
                    _readOperationUnderway = false;
                }
                Dispatcher.Invoke(() =>
                {
                    var saveFileDialog = new SaveFileDialog();
                    var showDialog = saveFileDialog.ShowDialog();
                    if (showDialog == false)
                        return;
                    using (var fs = new FileStream(saveFileDialog.FileName, FileMode.Create))
                    {
                        _flashBuffer.Flush();
                        fs.Write(_flashBuffer.GetBuffer(), 0, (int) _flashBuffer.Length);
                    }
                });
            }).Start();
        }

        private bool IsChecked(CheckBox cb)
        {
            if (cb.IsChecked == null)
                return false;
            return cb.IsChecked.Value;
        }

        private void WriteFuseLowButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("init");
            int low = 0;
            low |= IsChecked(CKSEL0CheckBox) ? 0 : 0x01;
            low |= IsChecked(CKSEL1CheckBox) ? 0 : 0x02;
            low |= IsChecked(CKSEL2CheckBox) ? 0 : 0x04;
            low |= IsChecked(CKSEL3CheckBox) ? 0 : 0x08;
            low |= IsChecked(SUT0CheckBox) ? 0 : 0x10;
            low |= IsChecked(SUT1CheckBox) ? 0 : 0x20;
            low |= IsChecked(BODENCheckBox) ? 0 : 0x40;
            low |= IsChecked(BODLEVELCheckBox) ? 0 : 0x80;
            SendData("writelowfuse 0x" + Convert.ToString(low, 16));
        }

        private void WriteFuseHighButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("init");
            int high = 0;
            high |= IsChecked(BOOTRSTCheckBox) ? 0 : 0x01;
            high |= IsChecked(BOOTSZ0CheckBox) ? 0 : 0x02;
            high |= IsChecked(BOOTSZ1CheckBox) ? 0 : 0x04;
            high |= IsChecked(EESAVECheckBox) ? 0 : 0x08;
            high |= IsChecked(CKOPTCheckBox) ? 0 : 0x10;
            high |= IsChecked(SPIENCheckBox) ? 0 : 0x20;
            high |= IsChecked(JTAGENCheckBox) ? 0 : 0x40;
            high |= IsChecked(OCDENCheckBox) ? 0 : 0x80;
            SendData("writehighfuse 0x" + Convert.ToString(high, 16));
        }

        private void WriteLockButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("init");
            int lck = 0;
            lck |= IsChecked(LB1CheckBox) ? 0 : 0x01;
            lck |= IsChecked(LB2CheckBox) ? 0 : 0x02;
            lck |= IsChecked(BLB01CheckBox) ? 0 : 0x04;
            lck |= IsChecked(BLB02CheckBox) ? 0 : 0x08;
            lck |= IsChecked(BLB11CheckBox) ? 0 : 0x10;
            lck |= IsChecked(BLB12CheckBox) ? 0 : 0x20;
            SendData("writelockbits 0x" + Convert.ToString(lck, 16));
        }

        private void EraseFlashButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("erase");
        }

        private void SerialInitButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("sinit");
        }

        private void SerialEndButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("send");
        }

        private void SerialWriteFuseLowButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("sinit");
            int low = 0;
            low |= IsChecked(CKSEL0CheckBox) ? 0 : 0x01;
            low |= IsChecked(CKSEL1CheckBox) ? 0 : 0x02;
            low |= IsChecked(CKSEL2CheckBox) ? 0 : 0x04;
            low |= IsChecked(CKSEL3CheckBox) ? 0 : 0x08;
            low |= IsChecked(SUT0CheckBox) ? 0 : 0x10;
            low |= IsChecked(SUT1CheckBox) ? 0 : 0x20;
            low |= IsChecked(BODENCheckBox) ? 0 : 0x40;
            low |= IsChecked(BODLEVELCheckBox) ? 0 : 0x80;
            SendData("swritelowfuse 0x" + Convert.ToString(low, 16));
        }

        private void SerialReadSignatureButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("ssig");
        }

        private void EndButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("end");
        }

        private void SerialEraseButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("serase");
        }

        private void SerialWriteFuseHighButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("sinit");
            int high = 0;
            high |= IsChecked(BOOTRSTCheckBox) ? 0 : 0x01;
            high |= IsChecked(BOOTSZ0CheckBox) ? 0 : 0x02;
            high |= IsChecked(BOOTSZ1CheckBox) ? 0 : 0x04;
            high |= IsChecked(EESAVECheckBox) ? 0 : 0x08;
            high |= IsChecked(CKOPTCheckBox) ? 0 : 0x10;
            high |= IsChecked(SPIENCheckBox) ? 0 : 0x20;
            high |= IsChecked(JTAGENCheckBox) ? 0 : 0x40;
            high |= IsChecked(OCDENCheckBox) ? 0 : 0x80;
            SendData("swritehighfuse 0x" + Convert.ToString(high, 16));
        }

        private void SerialWriteLockBitsButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("sinit");
            int lck = 0;
            lck |= IsChecked(LB1CheckBox) ? 0 : 0x01;
            lck |= IsChecked(LB2CheckBox) ? 0 : 0x02;
            lck |= IsChecked(BLB01CheckBox) ? 0 : 0x04;
            lck |= IsChecked(BLB02CheckBox) ? 0 : 0x08;
            lck |= IsChecked(BLB11CheckBox) ? 0 : 0x10;
            lck |= IsChecked(BLB12CheckBox) ? 0 : 0x20;
            SendData("swritelockbits 0x" + Convert.ToString(lck, 16));
        }

        private void SerialWriteFlashButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            var file = SelectedFileTextBox.Text;
            ReadElf readElf;
            try
            {
                var bytes = File.ReadAllBytes(file);
                readElf = new ReadElf(bytes);
                readElf.ReadFile();
            }
            catch (Exception)
            {
                MessageBox.Show("Could not read .elf file");
                return;
            }
            if (readElf.Memory.Count == 0)
            {
                Output("Nothing to program");
                return;
            }
            _writeOperationUnderway = true;
            new Thread(() =>
            {
                try
                {
                    for (int i = 0; i < readElf.Memory.Count; i += 128)
                    {
                        var b64 = Convert.ToBase64String(readElf.Memory.Skip(i).Take(128).ToArray());
                        SendData("strans " + b64);
                        _readOperationSemaphore.WaitOne(2000);
                        SendData("swrite " + i / 2);
                        _readOperationSemaphore.WaitOne(2000);
                    }
                }
                finally
                {
                    _writeOperationUnderway = false;
                }
            }).Start();
        }

        private void SerialReadFuses_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("sreadfuse");
        }

        private void SerialReadFlash_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            _flashBuffer.Seek(0, SeekOrigin.Begin);
            _flashBuffer.SetLength(0);
            _readOperationUnderway = true;
            new Thread(() =>
            {
                try
                {
                    //for (int page = 0; page < 20; page++)
                    for (int page = 0; page < 128; page++)
                    {
                        int address = page * 64;
                        SendData("sread " + address + " 64");
                        _readOperationSemaphore.WaitOne(2000);
                    }
                }
                finally
                {
                    _readOperationUnderway = false;
                }
                Dispatcher.Invoke(() =>
                {
                    var saveFileDialog = new SaveFileDialog();
                    var showDialog = saveFileDialog.ShowDialog();
                    if (showDialog == false)
                        return;
                    using (var fs = new FileStream(saveFileDialog.FileName, FileMode.Create))
                    {
                        _flashBuffer.Flush();
                        fs.Write(_flashBuffer.GetBuffer(), 0, (int)_flashBuffer.Length);
                    }
                });
            }).Start();
        }

        private void SerialVerifyFlash_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            ReadElf readElf;
            try
            {
                var file = SelectedFileTextBox.Text;
                var bytes = File.ReadAllBytes(file);
                readElf = new ReadElf(bytes);
                readElf.ReadFile();
            }
            catch (Exception)
            {
                Output("Could not read .elf");
                return;
            }
            if (readElf.Memory.Count == 0)
            {
                Output("Empty program");
                return;
            }
            _flashBuffer.Seek(0, SeekOrigin.Begin);
            _flashBuffer.SetLength(0);
            _readOperationUnderway = true;
            new Thread(() =>
            {
                try
                {
                    for (int page = 0; page < Math.Ceiling((double)readElf.Memory.Count / 128); page++)
                    {
                        int address = page * 64;
                        SendData("sread " + address + " 64");
                        _readOperationSemaphore.WaitOne(2000);
                    }
                }
                finally
                {
                    _readOperationUnderway = false;
                }
                var buffer = _flashBuffer.GetBuffer();
                if (readElf.Memory.Where((t, i) => t != buffer[i]).Any())
                {
                    Dispatcher.Invoke(() =>
                    {
                        MessageBox.Show("Verification failed");
                    });
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        MessageBox.Show("Verification successful");
                    });
                }
            }).Start();
        }

        private void VerifyFlashButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            ReadElf readElf;
            try
            {
                var file = SelectedFileTextBox.Text;
                var bytes = File.ReadAllBytes(file);
                readElf = new ReadElf(bytes);
                readElf.ReadFile();
            }
            catch (Exception)
            {
                Output("Could not read .elf");
                return;
            }
            if (readElf.Memory.Count == 0)
            {
                Output("Empty program");
                return;
            }
            _flashBuffer.Seek(0, SeekOrigin.Begin);
            _flashBuffer.SetLength(0);
            _readOperationUnderway = true;
            new Thread(() =>
            {
                try
                {
                    for (int page = 0; page < Math.Ceiling((double)readElf.Memory.Count / 128); page++)
                    {
                        int address = page * 64;
                        SendData("read " + address + " 64");
                        _readOperationSemaphore.WaitOne(2000);
                    }
                }
                finally
                {
                    _readOperationUnderway = false;
                }
                var buffer = _flashBuffer.GetBuffer();
                if (readElf.Memory.Where((t, i) => t != buffer[i]).Any())
                {
                    Dispatcher.Invoke(() =>
                    {
                        MessageBox.Show("Verification failed");
                    });
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        MessageBox.Show("Verification successful");
                    });
                }
            }).Start();
        }

        private void ResetButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                Output("Not connected");
                return;
            }
            SendData("reset");
        }

        private void TestButton_OnClick(object sender, RoutedEventArgs e)
        {
            var readBsdl = new ReadBsdl();
            readBsdl.Parse(@"C:\Users\Ronnie\Documents\Atmel Studio\7.0\Atmega16_Parallel_Programmer_SAM4E\ProgrammerWindowsApp\ProgrammerWindowsApp\LexicalParser\TestBsdl.txt");
        }
    }
}
